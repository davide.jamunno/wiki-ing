<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Favourite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class FavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $favourites = DB::table('favourites')
            ->select('favourites.*', 'articles.id', 'articles.title', 'articles.content', 'articles.user_id', 'users.id','users.username', 'name as category', 'articles.created_at as articleDate', 'articles.status as articleStatus', 'articles.status_comment')
            ->where('favourites.user_id', '=', Auth::user()->id)
            ->join('articles', 'favourites.article_id', '=' , 'articles.id')
            ->join('users', 'articles.user_id', '=', 'users.id')
            ->leftJoin('categories', 'articles.category_id', '=', 'categories.id')
            ->paginate(8);

        $readinglists = DB::table('readinglists')
            ->select('*')
            ->where('user_id', Auth::user()->id)
            ->get();
        
        return view('favourites.index',compact('favourites', 'readinglists'));
    }

     /**
     * Add the selected article into the favourite list.
     *
     * 
     */
    public function addToFavourites(Request $request)
    {
        $favourite_article = Article::findOrFail($request->id);
        

        Favourite::insert([
            'article_id' => $favourite_article->id,
            'user_id' => Auth::user()->id
        ]);

        return response()->json(
            [
              'success' => true,
              'message' => 'This article is now in your favourite list'
            ]
       );
    }

    /* removes one article from the favourites list */
    public function removeFromFavourites(Request $request)
    {
        $article_to_remove_id = Article::findOrFail($request->id)->id;
        
         DB::table('favourites')
                ->where('article_id', '=' , $article_to_remove_id) 
                ->delete();

        return response()->json(
            [
                'success' => true,
                'message' => 'This article was removed from your favourite list'
            ]
        );

    }
  
}