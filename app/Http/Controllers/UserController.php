<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('users.*')
            ->orderBy('id','desc')
            ->paginate(8);

        $categories = DB::table('categories')
            ->select('*')
            ->where('parent_category_id', null)
            ->get();
        
        return view('users.index',compact('users', 'categories'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = user::findOrFail($id);
        
        $categories = DB::table('categories')
            ->select('*')
            ->where('parent_category_id', null)
            ->get();
        return view('users.show', compact('user','categories'));  
    }

    //change status of an user
    public function changeUserStatus(Request $request)
    {
        
        $user = User::find($request->user_id);
        
        $user->status = $request->status;
        $user->save();
        
        return response()->json(
            [
              'success' => true,
              'message' => 'Status changed successfully'
            ]
       );

    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


   /*  public function favourites()
    {
        $user = Auth::user()->id;
        foreach ($user->articles as $article) {
            echo $article->pivot->created_at;
        }
    } */

    
}
