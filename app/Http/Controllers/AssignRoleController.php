<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Category;
use App\Models\Category_User;
use Illuminate\Support\Facades\DB;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AssignRoleController extends Controller
{ 
    //display assignRole form
    public function assignRoleCreate() 
    {
        //get information on categories
        $categories = DB::table('categories')
                ->select('*')
                ->get();
        
        //open view with form
        $roles = Role::all();
        $users = User::all();

        //select the user_id with the highest id (= last created user)
        $lastUserId = DB::table('users')
                ->select('username', 'id')
                ->max('id');

        $lastUserName = DB::table('users')
                ->select('username', 'id')
                ->where('id', '=', $lastUserId)
                ->get();
        
        //redirect to assignRoleStore
        return view('auth.assign-role', compact('users', 'roles', 'lastUserName', 'categories'));
        
    }

    //assign role to user
    public function assignRoleStore(Request $request) 
    {
        //collect information from $request
        $role_name = $request->input('role_name');
        $model_id = $request->input('model_id');
        $category_id = $request->input('category_id');

        //find user by id
        $user = User::find($model_id);
        
        //assign role to user
        $user->assignRole($role_name);

        //assign category to user
        DB::table('category_user')->insert([
            'category_id' => $category_id,
            'user_id' => $model_id
        ]);

        return redirect('users')->with('success','User Updated Successfully');

    }

    //remove role to user
    public function removeRole(Request $request) 
    {
        $idToModify = $request->input('user_id');
        $roleToRemove = $request->input('role');
        $user = User::find($idToModify);

        $user->removeRole($roleToRemove);

        return redirect('users')->with('success','User Updated Successfully');
    }
}
