<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Readinglist;
use App\Models\Article_Readinglist;
use App\Models\Readinglist_User;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Article;
use App\Models\Favourite;
use Illuminate\Support\Facades\DB;

class ReadinglistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $readinglists = Readinglist::select('readinglists.*', 'username')
            ->where('user_id', '=', Auth::user()->id )
            ->join('users', 'user_id', '=', 'users.id')
            /* ->join('readinglist_user', function ($join) {
                $join->on('readinglists.id', '=', 'readinglist_user.readinglist_id');
            }) */
            ->orderBy('name', 'asc')
            ->paginate(3);

        $articles = Article::select('title', 'articles.id', 'username')
            ->join('users', 'user_id', '=', 'users.id')
            ->get();

        $users = User::select('username', 'id')
            ->orderBy('username', 'asc')
            ->get();

        return view('readinglists.index',compact('readinglists', 'articles', 'users'));
    }

    /**
     * Display a listing of the resource. with shared lists
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSharedLists()
    {   
        $sharedLists = Readinglist_User::select('readinglist_id')
            ->where('user_id', '=', Auth::user()->id)
            ->pluck('readinglist_id')
            ->toArray();
        

        $readinglists = Readinglist::select('readinglists.*', 'username', 'comment', 'read')
            ->whereIn('readinglists.id', $sharedLists)
            ->join('users', 'user_id', '=', 'users.id')
            ->join('readinglist_user', function ($join) {
                $join->on('readinglists.id', '=', 'readinglist_id')
                     ->where('readinglist_user.user_id', '=', Auth::user()->id);
            })
            /* ->join('readinglist_user', 'readinglists.id', '=', 'readinglist_id') */
            ->orderBy('name', 'asc')
            ->paginate(3);
            

        $articles = Article::select('title', 'articles.id', 'username')
            ->join('users', 'user_id', '=', 'users.id')
            ->get();

        $users = User::select('username', 'id')
            ->orderBy('username', 'asc')
            ->get();

        return view('readinglists.index-shared',compact('readinglists', 'articles', 'users'));
    }


    /**
     * creates a new reading list and add the selected article 
     */
    public function createwitharticle(Request $request)
    {
        
        Readinglist::create([
            'name' => $request->readingListName,
            'user_id' => Auth::user()->id
        ]);

        $readinglist_id = Readinglist::latest()->first();
        
        Article_Readinglist::create([
            'article_id' => $request->article_id,
            'readinglist_id' => $readinglist_id['id']
        ]);


        return response()->json(
            [
              'message' => 'Reading List created successfully'
            ]
       ); 
    }

    /**
     * show the form for a new EMPTY reading list
     */
    public function create(Request $request)
    {

        return view('readinglists.create');
        
    }
    
    /* 
    save the new EMPTY reading list
     */
    public function store(Request $request)
    { 
        $request->validate([
        'name'=> 'required|max:100',
        ]);

        Readinglist::create([
            'name' => $request->name,
            'user_id' => Auth::user()->id
        ]);
    
        return redirect()->route('readinglists');

    }

    /* save one article into an existing reading list */
    public function saveArticle(Request $request)
    {
    
        $thisReadingList = Readinglist::find($request->listid);
        $hasArticle = $thisReadingList->articles()->where('id', $request->artid)->first();

        //if article is already in the list, inform the user and dont save it twice
        if($hasArticle === null){
            Article_Readinglist::create([
                'article_id' => $request->artid,
                'readinglist_id' => $request->listid
            ]);

            return response()->json(
                [
                    'message' => 'article saved correctly into the reading list!'
                ]
            );
        }else{
            return response()->json(
                [
                    'message' => 'article is already in the reading list, we dont save it twice...'
                ]
            );
        }

       /*  return response()->json(
            [
                'message' => 'article successfully saved in the reading list'
            ]
        ); */
    }

    /* delete the reading list */
    public function deleteList($id)
    {
        Readinglist::find($id)->delete($id);
        
            return response()->json([
                'action'=>'delete',
                'message'=>'reading list was successfully deleted',
                'errormsg' => 'sorry but this list cannot be deleted, try to unshare all users or delete the articles in it first!'
            ]);
        
    }

    /* remove one article from the reading list */
    public function removeFromList(Request $request)
    {
        $article_to_remove_id = Article::findOrFail($request->artid)->id;
        $readinglist_id = Readinglist::findOrFail($request->listid)->id;
        
        DB::table('article_readinglist')
                ->where('article_id', '=' , $article_to_remove_id)
                ->where('readinglist_id', '=', $readinglist_id)
                ->delete();

        return response()->json(
            [
                'success' => true,
                'message' => 'This article was removed from the list'
            ]
        );

    }

    /* permit to share the reading list with other users */
    public function shareList(Request $request)
    {
        $list_id = $request->list_id;
        $user_id = $request->user_id;
        $comment = $request->comment;

        $thisReadingList = Readinglist::find($list_id);
        $hasUser = $thisReadingList->users()->where('id', $user_id)->first();

        //if list is already shared with user, dont share twice and inform
        if($hasUser === null){
            Readinglist_User::create([
                "readinglist_id" => $list_id,
                "user_id" => $user_id,
                "comment" => $comment
            ]);

            return response()->json(
                [
                    'success' => 'true',
                    'message' => 'the list is now shared with the user!'
                ]
                );
        }else{
            return response()->json(
                [   
                    'success' => 'false',
                    'message' => 'list is already shared with this user!'
                ]
                );
        }
    }

    //delete or "detach" a readinglist from a user
    public function deleteListFromUser(Request $request)
    {   
        $list = Readinglist::where('id', $request->list_id)->firstOrFail();
        
            
        $list->users()->detach($request->user_id);

        return response()->json([
            'action'=>'delete',
            'message'=>'you removed the list correctly!'
        ]);

        
    }

    //set reading list as read
    public function readIt(Request $request)
    {
       
        User::find(Auth::user()->id)->readinglists()->updateExistingPivot($request->list_id, ['read' => $request->read]);

        return response()->json([
            
            'message'=>'you read the list, congratulations!'
        ]);

    }

}
