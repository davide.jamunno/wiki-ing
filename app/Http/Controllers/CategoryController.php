<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Favourite;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $categories = DB::table('categories')
                ->select('*')
                ->get();
        
        return view('categories.index',compact('categories'));
    }

    /**
     * Display a listing of the resource with parent category === 0.
     *
     * @return \Illuminate\Http\Response
     */
    public function parentZero()
    {   
        $categories = DB::table('categories')
                ->select('*')
                ->where('parent_category_id', '=', 0)
                ->get();
        
        return view('categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validieren
        $request->validate([
            'name'=> 'required|max:100',
            
        ]);
        
        //write into the db
         Category::create([
            'name' => $request->name,
        ]);
        //show message in form
        return redirect()->route('categories')->with('success', 'ok, we have now a new category, hurray!');
    }

    //add a new category to the user
    public function addCategoryToUser(Request $request) 
    {
        //collect information from $request
       
        $user_id = $request->input('user_id');
        $category_id = $request->input('choosenCat');

        //finds out which categories has user
        $user_cat = DB::table('category_user')
                ->select('*')
                ->leftJoin('users', 'category_user.user_id', '=', 'users.id')
                ->where('users.id', '=', $user_id)
                ->pluck('category_id')
                ->toArray();

        if(in_array($category_id, $user_cat))
        {
            return response()->json([
                'status' => 'fail',
                'message'=>'user has already this category!!'
            ]);
            }else
            {
                //assign category to user
                DB::table('category_user')->insert([
                    'category_id' => $category_id,
                    'user_id' => $user_id
                ]);
                return response()->json([
                    'status' => 'success',
                    'message'=>'you successfully added the category!'
                ]);
            }
    }

    //delete or "detach" a category from a user
    public function deleteCategoryFromUser(Request $request)
    {   
        $cat = Category::where('name', $request->category_name)->firstOrFail();
        
            
        $cat->users()->detach($request->user_id);

        return response()->json([
            'action'=>'delete',
            'message'=>'you removed the category correctly!'
        ]);

        
    }
}
