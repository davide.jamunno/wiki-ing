<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Favourite;
use App\Models\Category;
use App\Models\Upload;
use App\Models\Article_Upload;
use App\Models\Article_Readinglist;
use App\Models\Readinglist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;


class ArticleController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //creates an array of all articles id in the fav list of the auth user
        $userFav = DB::table('favourites')
                ->select('favourites.article_id')
                ->leftJoin('users', 'favourites.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('article_id')
                ->toArray();

        $users = User::all();

        //creates an array of all fields where the auth user is active
        $user_fields = DB::table('category_user')
                ->select('*')
                ->leftJoin('users', 'category_user.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('category_id')
                ->toArray();

        $articles = DB::table('articles')
                ->select('articles.id','title', 'content', 'articles.status as articleStatus','username', 'articles.created_at', 'users.id as authUser')
                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
                ->where('articles.status', '=', '1') 
                ->whereIn('articles.category_id', $user_fields)
                ->where(function($query)  {
                    $query->where('title', 'like', "pisto")
                        ->orWhere('content', 'like', "bla");
                })
                ->orderBy('username', 'asc')
                ->paginate(5);
                /* ->get(); */
        
        return view('articles.index',compact('articles', 'users', 'userFav', 'user_fields'));
    }
    /**
     * Display a listing of the resource written by the authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function myArticles()
    {   
        
        $articles = DB::table('articles')
                ->select('articles.id','title', 'content', 'name as category', 'articles.status as articleStatus','username', 'articles.created_at as articleDate')
                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
                ->leftJoin('categories', 'articles.category_id', '=', 'categories.id')
                ->where('users.id', '=', Auth::user()->id)
                ->latest('articleDate')
                ->paginate(8);

        $readinglists = DB::table('readinglists')
                ->select('*')
                ->where('user_id', Auth::user()->id)
                ->get();
        
        return view('articles.myArticles', compact('articles', 'readinglists'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //fins out which fields can write the user in
        $user_fields = DB::table('category_user')
                ->select('*')
                ->leftJoin('users', 'category_user.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('category_id')
                ->toArray();


        $categories = DB::table('categories')
                ->select('*')
                ->whereIn('id', $user_fields)
                ->get();
        return view('articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        /* dd(phpinfo()); */
        //validieren
        $request->validate([
            'title'=> 'required|max:100',
            'content'=>'required',  
            'status' => 'required|boolean',
            'category' => 'required|integer',
            'uploads.*'=> 'image|max:3000'
        ]);
        
        
        /* dd($request->file('uploads')); */
        //write into the db
        $article = Article::create([
            'title' => $request->title,
            'content' => $request->content,
            'status' => $request->status,
            'user_id' => Auth::user()->id,
            'category_id' => $request->category
        ]);

        //saving pictures in folder public uploads
        $files = [];

        
        
        
        if($request->hasfile('uploads'))
        {
            foreach($request->file('uploads') as $file)
            {
                $name = $file->getClientOriginalName();
                $type = $file->getClientMimeType();
                $size = $file->getSize();
                $file->move(public_path('uploads'), $name);  
                //for deploy --->   move('uploads', $name)
                $files[] = ['name' => $name, 
                            'type' => $type,
                            'size' => $size]; 
            }
        }

        
         
        //saving info in db
        $uploadsArr = [];
        foreach($files as $key => $file){
           
            $upload = Upload::create([
                'name' => $file['name'],
                'file_type' => $file['type'],
                'file_size' => $file['size'],
                'user_id' => Auth::user()->id,
            ]);
            $uploadsArr[] = $upload;

            
        }
        
        //saving connection article / uploads

        foreach($uploadsArr as $upload){
            Article_Upload::create([
                'article_id' => $article->id,
                'upload_id' => $upload->id
            ]);
        };


        //show message and redirect
        return redirect()->route('article.show', $article->id)->with('success', 'thanks for writing your article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //creates an array of all fields where the auth user is active
        $user_fields = DB::table('category_user')
            ->select('*')
            ->leftJoin('users', 'category_user.user_id', '=', 'users.id')
            ->where('users.id', '=', Auth::user()->id)
            ->pluck('category_id')
            ->toArray();

        $article = Article::where('articles.id', '=', $id)
            ->whereIn('articles.category_id', $user_fields)
            ->select('articles.id','articles.title', 'articles.content', 'articles.status','users.username', 'articles.created_at as articleDate', 'articles.category_id as articleCategory')
            ->leftJoin('users', 'articles.user_id', '=', 'users.id')
            ->findOrFail($id);
        
        $littleInfo = Article::where('articles.id', '=', $id)
            ->whereIn('articles.category_id', $user_fields)
            ->select('articles.id','articles.title', 'articles.status','users.username', 'articles.created_at as articleDate', 'articles.category_id as articleCategory', 'articles.status_comment')
            ->leftJoin('users', 'articles.user_id', '=', 'users.id')
            ->findOrFail($id);

        $userFav = DB::table('favourites')
                ->select('favourites.article_id')
                ->leftJoin('users', 'favourites.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('article_id')
                ->toArray(); 

        $readinglists = DB::table('readinglists')
            ->select('*')
            ->where('user_id', Auth::user()->id)
            ->get();

        //getting the pictures 
        $uploads = Article::findOrFail($id)
            ->uploads()->get();
        

        if($article->status == 1 || Auth::user()->id == $article->user_id || Auth::user()->hasRole('super-admin') == true ) {
            return view('articles.show', compact('article', 'userFav', 'readinglists', 'uploads'));  
        }else 
        {
            return view('articles.error', compact('littleInfo'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = DB::table('articles')
            ->where('articles.id', '=', $id)
            ->select('articles.id','articles.title', 'articles.content', 'articles.status','users.username', 'articles.created_at as articleDate')
            ->leftJoin('users', 'articles.user_id', '=', 'users.id')
            ->get();

        $uploads = Article::findOrFail($id)
            ->uploads()->get();

        return view('articles.edit', compact('article', 'uploads'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validieren
        $editable = $request->validate([
            'title'=>'required|max:100',
            'content'=>'required', 
            'status'=>'boolean',     
        ]);

        $request->validate([
            'uploads.*'=>'image|max:3000', 
        ]);

        //write into the db
        Article::whereId($id)->update($editable);

        //saving pictures in folder public uploads
        $files = [];
        
        if($request->hasfile('uploads'))
        {
            foreach($request->file('uploads') as $file)
            {
                $name = $file->getClientOriginalName();
                $type = $file->getClientMimeType();
                $size = $file->getSize();
                $file->move(public_path('uploads'), $name);  
                $files[] = ['name' => $name, 
                            'type' => $type,
                            'size' => $size]; 
            }
        }
         
        //saving info in db
        $uploadsArr = [];
        foreach($files as $key => $file){
           
            $upload = Upload::create([
                'name' => $file['name'],
                'file_type' => $file['type'],
                'file_size' => $file['size'],
                'user_id' => Auth::user()->id,
            ]);
            $uploadsArr[] = $upload;

            
        }
        
        //saving connection article / uploads

        foreach($uploadsArr as $upload){
            Article_Upload::create([
                'article_id' => $id,
                'upload_id' => $upload->id
            ]);
        };


        //show message in form
        return redirect()->route('article.show', $id)->with('success', 'thanks for updating the article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'id'=>'required', 
        ]);

        $id = $request->id;
        //check if article is in favourite list
        $favourites = Favourite::select('user_id', 'username')
                ->where('article_id', $id)
                ->leftJoin('users', 'favourites.user_id', '=', 'users.id')
                ->pluck('username')
                ->toArray();

        $readinglists = Article_Readinglist::select('username', 'readinglists.name')
                ->where('article_id', $id)
                ->leftJoin('readinglists', 'article_readinglist.readinglist_id', '=', 'readinglists.id')
                ->leftJoin('users', 'readinglists.user_id', '=', 'users.id')
                ->pluck('username', 'readinglists.name')
                ->toArray();
        
                
        $string = implode(", ", $favourites);
                
        $stringList = '';
        
        foreach($readinglists as $key => $value){
            $stringList .= $value . " with the list name: '". $key . "'. ";
            
        };

        



        //check if the article is in a reading list
        /* $readinglists = DB::table('article_readinglist')
                ->select('article_id')
                ->pluck('article_id')
                ->toArray(); */

        //creates an array with all the pictures ids
        $imagesId = DB::table('article_upload')
                ->where('article_id', $id)
                ->select('upload_id')
                ->pluck('upload_id')
                ->toArray();

        //check if the article is saved in a list(favourites or readinglist)
        if(!empty($favourites))
        {
            return response()->json([
                'action' =>'wait',
                'message' => 'This article cannot be deleted. This is because it is saved in Favourites by following user/s: '.$string,
                
            ]);
        } elseif(!empty($readinglists)) {
            return response()->json([
                'action' =>'wait',
                'message' => 'This article cannot be deleted. This is because it is saved in Reading List/s by following user/s: '.$stringList,
  
            ]);
        }
        else {
            //deletes article
            Article::find($id)->delete($id);
            
            if($imagesId){
                foreach($imagesId as $imgid){
                    //deletes images in upload table
                    Upload::find($imgid)->delete($imgid);
                    //deletes pivot table data
                    DB::table('article_upload')
                        ->where('article_id', '=' , $id)
                        ->where('upload_id', '=' , $imgid)
                        ->delete();
                }
            }
        

            return response()->json([
                'action'=>'delete',
                'message'=>'article was successfully deleted'
            ]);
        }     
    }

    //just for admin, a list of all articles with status set to 0(inactive articles)
    public function statusZeroList()
    {   
        //creates an array of all articles id in the fav list of the auth user
        $userFav = DB::table('favourites')
                ->select('favourites.article_id')
                ->leftJoin('users', 'favourites.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('article_id')
                ->toArray();   
        $articles = DB::table('articles')
                ->select('articles.id','title', 'content', 'articles.status as articleStatus','username', 'articles.created_at', 'users.id as authUser', 'status_comment')
                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
                ->where('articles.status', '=', '0')
                ->orderBy('username', 'asc')
                ->paginate(5);
        
        return view('articles.statusZero',compact('articles', 'userFav'));
    }

    //search for a specific article
    public function search(Request $request)
    {
        $key = trim($request->input('q'));
        $cat = 'pesce';

        //creates an array of all fields where the auth user is active
        $user_fields = DB::table('category_user')
                ->select('*')
                ->leftJoin('users', 'category_user.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('category_id')
                ->toArray();

        //get the main gategories
        $categories = DB::table('categories')
                ->select('*')
                ->where('parent_category_id', '=', null)
                ->whereIn('id', $user_fields)
                ->get();

        $articles = DB::table('articles')
                ->select('articles.id', 'articles.status as articleStatus', 'name as category', 'title', 'articles.created_at as articleDate', 'username', 'status_comment', )
                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
                ->leftJoin('categories', 'articles.category_id', '=', 'categories.id')
                //it works
                ->where('articles.status', '=', 1)
                //only articles that are in the user fields of the registered user
                //does it work?test!
                ->where(function($query) use($user_fields) {
                    $query->whereIn('articles.category_id', $user_fields);
                })
                //it works
                ->where(function($query) use ($key) {
                    $query->where('title', 'like', "%{$key}%")
                          ->orWhere('content', 'like', "%{$key}%")
                          ->orWhere('username', 'like', "%{$key}%");
                })
                ->orderBy('id', 'desc')
                ->paginate(8);
                

        //save the query string (q=....) after the first page of pagination
        $articles->appends($request->all());
        /* $all_articles->appends($request->all()); */
        
        $userFav = DB::table('favourites')
                ->select('favourites.article_id')
                ->leftJoin('users', 'favourites.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('article_id')
                ->toArray();  
       
        //get the recent 5 articles
        $recent_articles = Article::query()
                ->orderBy('articles.created_at', 'desc')
                ->take(5)
                ->get();

        $readinglists = DB::table('readinglists')
                ->select('*')
                ->where('user_id', Auth::user()->id)
                ->get();

        return view('articles.search', [
            'cat' => '',
            'key' => $key,
            'all_articles' => $articles,
            'recent_articles' => $recent_articles,
            'userFav'=> $userFav,
            'user_fields' => $user_fields,
            'categories' => $categories,
            'readinglists' => $readinglists,
            /* 'all_articles' => $all_articles */
        ]);
    }

    public function searchByCategory(Request $request)
    {  
        
     /*    $category = Category::where('categories.id', '=', $cat)
            ->select('*')
            ->findOrFail($cat); */
            
        
        $cat = $request->cat;

        $user_fields = DB::table('category_user')
            ->select('*')
            ->leftJoin('users', 'category_user.user_id', '=', 'users.id')
            ->where('users.id', '=', Auth::user()->id)
            ->pluck('category_id')
            ->toArray();
        
        $articles = DB::table('articles')
                ->select('articles.id', 'articles.status as articleStatus' , 'title', 'articles.created_at as articleDate', 'username', 'name as category', 'status_comment')
                ->leftJoin('users', 'articles.user_id', '=', 'users.id')
                ->leftJoin('categories', 'articles.category_id', '=', 'categories.id')
                ->where('articles.status', '=', 1)
                ->where('articles.category_id', '=', $cat)
                ->where(function($query) use($user_fields) {
                    $query->whereIn('articles.category_id', $user_fields);
                })
                ->orderBy('id', 'desc')
                ->paginate(8);

        //save the query string (q=....) after the first page of pagination
        $articles->appends($request->all());
        
        $userFav = DB::table('favourites')
                ->select('favourites.article_id')
                ->leftJoin('users', 'favourites.user_id', '=', 'users.id')
                ->where('users.id', '=', Auth::user()->id)
                ->pluck('article_id')
                ->toArray();  
        //get the main gategories
        $categories = DB::table('categories')
                ->select('*')
                ->where('parent_category_id', '=', null)
                ->whereIn('id', $user_fields)
                ->get();
        
        /* $key = DB::table('categories')
                ->select('categories.name')
                ->where('id', '=', $cat)
                ->pluck('name')
                ->implode('name', '['); */

        
        
        $readinglists = DB::table('readinglists')
                ->select('*')
                ->where('user_id', Auth::user()->id)
                ->get();
                
        return view('articles.search', [           
            'all_articles' => $articles,
            'cat' => $cat,
            'userFav'=>$userFav,
            'categories'=>$categories,
            'readinglists' => $readinglists,   
              
        ]);

        /* return response()->json([
            'articles' =>$articles,
            'cat' => $cat,
            'userFav'=>$userFav,
            'categories'=>$categories,
            'readinglists' => $readinglists,
        ]); */
    }

    //change status of an article
    public function changeArticleStatus(Request $request)
    {
        
        $article = Article::find($request->article_id);
        $article->status = $request->status;
        $article->save();
        
        return response()->json(
            [
              'success' => true,
              'message' => 'Status changed successfully'
            ]
       );
    }

    public function setArticleStatusToOne(Request $request)
    {
        
        $article = Article::find($request->id);
        $article->status = 1;
        $article->save();
        
        return response()->json(
            [
              'success' => true,
              'message' => 'this article is now "active"'
            ]
       );
    }

    //change status message of an article
    public function changeArticleComment(Request $request)
    {
        /* print_r($request); */
        $editable = $request->validate([
            'status_comment' => 'required',
            'status' => 'required|boolean'
        ]);
        Article::whereId($request->id)->update($editable);
        
        return response()->json(
            [
              'success' => true,
              'message' => 'changed successfully'
            ]
       );
    }

    //upload images with tinyMce
    public function upload_image(Request $request)
    {
        $mainImage = $request->file('file');
        $filename = time() . '.' . $mainImage->extension();
        Image::make($mainImage)->save(public_path('tinymce_images/' . $filename));

        return json_encode(['location' => asset('tinymce_images/' . $filename)]);
    }

    //delete a picture in edit mode
    public function delete_picture(Request $request)
    {
        $imageId = $request->pictureid;
        $articleId = Article::findOrFail($request->artid)->id;

        Upload::find($imageId)->delete($imageId);

        
        DB::table('article_upload')
                ->where('article_id', '=' , $articleId)
                ->where('upload_id', '=', $imageId)
                ->delete();


        return response()->json(
            [
                'success' => true,
                'message'=> 'this picture was deleted'
            ]
        );
    }

}
