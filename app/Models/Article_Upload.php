<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article_Upload extends Model
{
    protected $table = 'article_upload';
    protected $fillable = [
        'article_id', 'upload_id'
    ];
    public $timestamps = false;
}