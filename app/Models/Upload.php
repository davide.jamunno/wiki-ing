<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *	
     * @var array
     */
    protected $fillable = [
        'name', 'file_type', 'file_size', 'user_id'
    ];

    /**
     * access articles table
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    
}