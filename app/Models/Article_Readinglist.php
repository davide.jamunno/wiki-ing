<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article_Readinglist extends Model
{
    protected $table = 'article_readinglist';
    protected $fillable = [
        'article_id', 'readinglist_id'
    ];
    public $timestamps = false;
}