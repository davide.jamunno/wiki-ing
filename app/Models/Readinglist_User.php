<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Readinglist_User extends Model
{
    protected $table = 'readinglist_user';
    protected $fillable = [
        'user_id', 'readinglist_id', 'comment', 'read'
    ];
    public $timestamps = false;
}