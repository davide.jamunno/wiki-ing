<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Readinglist extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'name'
    ];

    /**
     * access articles table 
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }
    /**
     * access users table 
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

     /**
     * access users table 
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('read');
    }

    
}
