<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'content' , 'user_id','status', 'category_id'
    ];

    /**
     * access user table
     */
    public function users()
    {
        return $this->hasOne(User::class);
    }

    /**
     * access favourites table
     */
    public function favourites()
    {
        return $this->hasMany(Favourite::class);
    }

    /**
     * access reading list table 
     */
    public function lists()
    {
        return $this->belongsToMany(Readinglist::class);
    }

     /**
     * access uploads table
     */
    public function uploads()
    {
        return $this->belongsToMany(Upload::class);
    }

    
}
