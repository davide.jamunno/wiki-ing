<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9">
        </div>
    </x-slot>
    <div class="m-3 mx-auto max-w-3xl">

        <div class="border border-gray-400 bg-white rounded-sm p-4 flex flex-col justify-between ">
            <div class="mb-8">
        
                <h2>Sorry, the article <b>"{{ $littleInfo->title }}"</b> is not active and you are not able to read it. </h2>

                @if (!$littleInfo->status_comment == '')
                <p>Admin has written this comment:</p>
                <h2>{{ $littleInfo->status_comment }}</h2> 

                @endif
            </div>
        </div>
        
    </div>  
    
                
            
        
    

        

</x-app-layout>