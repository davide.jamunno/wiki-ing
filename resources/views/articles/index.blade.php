<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Articles') }}
        </h2>
    </x-slot>
    
   
    @if ($message = Session::get('success'))
        <div class="p-6 bg-white border-b border-gray-200 flex justify-between">
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative">
                <p>{{ $message }}</p>
            </div> 
        </div>
    @endif

                <div class="px-8 py-1 md:w-full">
                    <table class="table-auto text-left md:w-full">
                      <thead class="justify-between">
                        <tr class="bg-blue-800">
                          <th class="px-16 py-2">
                            <span class="text-gray-300">Title</span>
                          </th>
                          <th class="px-16 py-2">
                            <span class="text-gray-300">Author</span>
                          </th>
                          <th class="px-16 py-2">
                            <span class="text-gray-300">Created on</span>
                          </th>
                          <th class="px-16 py-2">
                            <span class="text-gray-300">Actions</span>
                          </th>
                        </tr>
                      </thead>

                      <tbody class="bg-gray-200">
                        @forelse ($articles as $article)
                        <tr class="{{ $article->id }} bg-white border-4 border-gray-200">
                          <td class="px-16 py-2">
                              <a href="{{ 'articles/'.$article->id }}">{{ $article->title }}</a>
                          </td>
                          <td class="px-16 py-2">
                              {{ $article->username }}
                          </td>
                          <td class="px-16 py-2">
                              {{ $article->created_at}}
                          </td>
                          <td class="px-16 py-2 flex items-center space-x-4">

                          {{-- checking if the article is already in favourite ist and the display the right button accordingly --}}
                           @php

                               if(in_array("$article->id", $userFav))
                               {
                                   echo '<button class="removeFromFavourites removeFromFav'.$article->id.'" data-id='.$article->id.'><i class="fas fa-heart" title="remove from favourites"></i></i></button>';
                               } else {
                                   echo '<button class="saveFavourite saveToFav'.$article->id.'" data-id='.$article->id.'><i class="far fa-heart" title="add to favourites"></i></button>';
                               }
                           @endphp
                            

                      {{--       @can('delete articles')
                                    <button class="deleteRecord" data-id="{{ $article->id }}" data-title="{{ $article->title }}" ><i class="fas fa-trash-alt" title="delete"></i></button>

                                    <button class="setStatusToZero" data-id="{{ $article->id }}" data-title="{{ $article->title }}" ><i class="fas fa-eye-slash" title="set status to Zero"></i></button>



                            @endcan --}}
            
                            @can('edit articles')
                                    <a href="{{ route('article.edit',$article->id) }}"><i class="fas fa-pen" title="edit"></i></a>
                            @endcan
                            </td>
                            <td class="px-16 py-2">
                                @can('delete articles')
                                    <div class="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in">
                                        <input data-id="{{$article->id}}" class="toggle-class toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer" name="toggle" type="checkbox"  data-toggle="toggle" data-on="Active" data-off="InActive" {{ $article->articleStatus ? 'checked' : '' }}>
                                        <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>
                                        
                                    </div>
                                        
                                
                                @endcan
                            </td>
                        </tr>
                        @empty
                            <p>There are no articles here,  <a class="underline" href="{{ route('article.create') }}">click here </a>to write an article</p>        
                        @endforelse
                      </tbody>
                    </table>
                </div>
                
                <div class="m-8">
                    {{ $articles->links() }}
                </div>
                

<script>

    //delete selected article
$(".deleteRecord").click(function()
{    
    var title = $(this).data("title");
  
    //check if you really want to delete
      if(!confirm("delete "+title)){return false};
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
                    
    
        $.ajax(
        {
            url: "articles/"+id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
                //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                }).showToast();

                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $("."+id).remove(); 
                } 

            }   
        });
    
}
);



//save selected article into favourites list
$(document).on('click', '.saveFavourite', function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax(
        {
            url: "favourites/"+id,
            type: 'POST',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();

            //change the fav button?
            $(".saveToFav"+id).replaceWith('<button class="removeFromFavourites removeFromFav'+id+'" data-id="'+id+'"><i class="fas fa-heart" title="remove from favourites"></i></button>');

                
        },
        error:function(error){
            console.log(error)
        }
    });

}
);


$(document).on('click','.removeFromFavourites',function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
                    
    $.ajax(
    {
        url: "favourites/"+id,
        type: 'DELETE',
        data: {
            "id": id,
            "_token": token,
        },
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();

            //change the fav button?
            $(".removeFromFav"+id).replaceWith('<button class="saveFavourite saveToFav'+id+'" data-id="'+id+'"><i class="far fa-heart" title="add to favourites"></i></button>');
        }
    });  
}
);

//set article status to 0
$(document).on('click','.setStatusToZero',function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
         
    $.ajax(
    {
        url: "articles/change-status",
        type: 'GET',
        data: {
            "id": id,
            "_token": token,
            "status": status
        },
    
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();

            //change the icon ?
            
        }
    });  
}
);

//change status of article with a toggle 
$(function() {
    $('.toggle-class').change(function() {
        
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var article_id = $(this).data('id'); 
         
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('changeArticleStatus') }}',
            data: {
                'status': status, 
                'article_id': article_id
            },
            success: function(data){
              console.log(data.success)
            }
        });
    })
});


    </script>
</x-app-layout>
