<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9 px-1 pt-1 text-sm font-medium leading-5 text-gray-900 ">{{ $article[0]->title }} / edit</div>
    </x-slot>
    @push('head')
    <script src='https://cdn.tiny.cloud/1/lw0e28qxdhyde165cweawoc2gdzor1j4njh1aiqapby8wqxm/tinymce/5/tinymce.min.js' referrerpolicy="origin">
    </script>
    <style>
        .tox-notifications-container{
            display:none;
        }
    </style>
    @endpush

<div class="m-3 mx-auto max-w-3xl">
    @if ($errors->any())
    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form class="flex flex-col" action="{{ route('article.patch',$article[0]->id) }}" method="post" enctype="multipart/form-data">

        {{-- verify that the token in the request input matches the token stored in the session. When these two tokens match, we know that the authenticated user is the one initiating the request. --}}
        @csrf
        {{-- hidden method --}}
        @method('PATCH')
        
        {{-- title --}}
        <div class="m-auto mb-2">
            <label class="block text-gray-500 font-bold" for="title">title:</label>
        </div>
            
        <div class="m-auto mb-12">
            <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700" type="text" class="form-control" name="title" value="{{ $article[0]->title }}">
        </div>
        
    
        {{-- content --}}
        <div class="m-auto mb-2">
            <label class="block text-gray-500 font-bold" for="content">content:</label>
        </div>
        <div class="m-auto min-w-full mb-6">
            <div class="min-w-full mb-12">
                <textarea id="mytextarea" class="" rows="15" name="content">{{ $article[0]->content }}</textarea>
            </div>
        </div>
        
        

        {{-- picture preview --}}
        <div class="m-auto mb-2">
            <label class="block text-gray-500 font-bold" for="">edit article's pictures...</label>
        </div>
       
        <div class="flex items-center justify-between space-x-4">
        
            @foreach ($uploads as $picture)
                <div class="mx-2 h-48 w-48 mt-2 border-4 relative border-gray-80 pictureid{{ $picture->id }}">
                    <a href="#" class="mr-1 deletePicture absolute top-0 right-0 transition-all transform hover:scale-150" data-artid="{{ $article[0]->id }}" data-pictureid="{{ $picture->id }}" title="delete picture"><i class="fas fa-minus-circle text-gray-600 hover:text-red-600 z-10"></i></a>
                    <img src="{{asset('uploads/' . $picture->name)}}" alt="{{ $picture->name }}" class="object-cover h-full w-full"> 
                </div>
            @endforeach
    
        </div>

        {{-- picture upload file and preview--}}
        <div class="m-auto mb-2 mt-4">
            <label class="block text-gray-500 font-bold" for="">...or upload new ones!</label>
        </div>
        <div class="containerOfPictures m-auto flex flex-wrap mb-2">
            <div class="toRemoveW h-40 w-40  m-1 p-0">
                <label class="cursor-pointer hover:bg-gray-300 bg-gray-200 flex h-40 w-40 block relative m-0 p-0" for="ett">
                    
                        <div class="m-auto flex flex-col align-content items-center ">
                            <i class="text-4xl align-content items-center folder toShow fas fa-folder-open text-gray-500"></i>
                            <p class="text-gray-500 align-content items-center font-bold">click here<br>to upload</p>
                        </div>
                        
                        <input id="ett" type="file" name="uploads[]"  oninput="previewFile(this)" class="hidden">
                   
                        <img class=" absolute   left-0 right-0 align-content items-center object-cover h-full w-full" />
                    
                </label>
            </div>
        </div>

        <div class="flex m-auto">
            <div class="m-auto mb-12">
                <button class="m-auto newPic text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded">add another picture</button>
            </div>
            <div class="m-auto mb-12">
                <button id="cancel" class="m-auto text-md border border-red-400 text-red-400 hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded">cancel pictures</button>
            </div>
        </div>

        {{-- status --}}
        {{-- check if user is authorized to change status to article --}}
        @if ($article[0]->username === Auth::user()->username || Auth::user()->hasrole('super-admin') === true )


        <div class="mb-12 m-auto">
            <div class="flex">
                <div class="flex items-center">
                    <input class="mr-2 leading-tight" type="radio"  value="0" name="status" checked>
                    <label class="mr-4 text-gray-500 font-bold" for="0">save as draft</label>
                </div>
                <div class="items-center flex">
                    <input class="mr-2 leading-tight" type="radio"  value="1" name="status">
                    <label class="block text-gray-500 font-bold" for="1">publish</label>
                </div>
            </div>
            
        </div>
        @endif

        {{-- send form --}}
        <div class="mb-10 m-auto">
            <div class="w-full">
                
                <button type="submit" class="text-xl w-32 text-white font-bold bg-gradient-to-r from-logogreen to-pink-400 hover:from-pink-500 hover:to-yellow-500 mx-1 px-3 py-1 rounded">Go!</button>
            </div>
        </div>
    
        
    </form>
</div>


<script>
    var editor_config = {
        path_absolute: "/",
        selector: "#mytextarea",
        directionality: document.dir,
        plugins: [
                "autolink lists",
                "wordcount",
            ],
        toolbar: "insertfile undo redo | formatselect | bold italic strikethrough | alignleft aligncenter alignright | bullist ",
        relative_urls: false,
        
        
    };

    tinymce.init(editor_config);

//delete picture
$('.deletePicture').on('click', function(event)
{   
    if(!confirm("delete this picture?")){return false};
    var artid = $(this).data("artid");
    var pictureid = $(this).data("pictureid");
    var token = $("meta[name='csrf-token']").attr("content");
                    
    $.ajax(
    {
        url: "{{ route('deletePicture')}}",
        type: 'POST',
        data: {
            "artid": artid,
            "pictureid":pictureid,
            "_token": token,
        },
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();

            //delete from DOM
            $(".pictureid"+pictureid).fadeOut();
        }
    });  
    event.preventDefault();
});

//img preview
var counter = 1;

function previewFile(input) {
    var preview = input.nextElementSibling;
    var file = input.files[0];

    //check img size, if greater than 2mb alert and return!
    if(file.size > 3145728){
        alert("please upload a picture smaller than 3 MB!");
        input.value = "";
        return
    };

    var reader = new FileReader();
    /* $(".folder").hide(); */
    reader.onloadend = function() {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
        
        /* $(".folder").show(); */

    }
}

//creates a new input files
$(".newPic").click(function(e)
{   e.preventDefault();
var html = "<div class='m-1 toRemoveW h-40 w-40 '><label class='cursor-pointer hover:bg-gray-300 bg-gray-200 flex h-40 w-40 m-0 p-0 block relative' for=ett"+counter+"><div class='m-auto flex flex-col align-content items-center'><i class='text-4xl align-content items-center folder toShow fas fa-folder-open text-gray-500'></i><p class='text-gray-500 align-content items-center font-bold'>click here<br>to upload</p></div><input id=ett"+counter+" type='file' name='uploads[]' oninput='previewFile(this)' class='hidden'><img class='absolute object-cover h-40  max-h-40 m-auto  left-0 right-0 align-content items-center' /></label></div>";

    
$(".containerOfPictures").append(html);

counter = counter + 1;  
});

//cancel pictures sumbission
$("#cancel").click(function(e){
    e.preventDefault();
    $(".toRemoveW").remove();

});


</script>

</x-app-layout>