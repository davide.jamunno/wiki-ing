<x-app-layout>
    <x-slot name="header">
        <div class="hidden sm:flex items-center flex-start h-9">
            @can('manage users')
                <x-nav-link :href="route('users')" :active="request()->routeIs('users')">
                    {{ __('Manage users') }}
                </x-nav-link>
                @endcan
            @can('delete articles')
                <x-nav-link :href="route('statusZero')" :active="request()->routeIs('statusZero')">
                    {{ __('Inactive articles') }}
                </x-nav-link>
            @endcan

            @can('create categories')
                <x-nav-link :href="route('categories')" :active="request()->routeIs('categories')">
                    {{ __('Categories') }}
                </x-nav-link>
            @endcan
            
        </div>
    </x-slot>

        <div class="max-w-7xl mx-auto pb-3 px-4 sm:px-6 lg:px-8">
            <table class=" text-left w-full">
                <div class="border-b-4 border-pink-200"></div>
                {{-- <thead class="justify-between">
                    <tr class="text-lg text-white font-bold bg-gradient-to-r from-logogreen to-pink-400">
                        <th class="px-8 py-2">
                            <span>Title</span>
                        </th>
                        <th class="px-8 py-2 hidden sm:table-cell">
                            <span>Author</span>
                        </th>
                        <th class="px-8 py-2 hidden sm:table-cell">
                            <span>Created on</span>
                        </th>
                        <th class="px-8 py-2">
                            <span>Actions</span>
                        </th>
                        <th class="px-8 py-2 hidden sm:table-cell">
                            <span>Status Comment</span>
                        </th>
                    </tr>
                </thead> --}}

                <tbody class="bg-gray-200">
                    @forelse ($articles as $article)
                        <tr class="{{ $article->id }} bg-white border-b border-gray-200">
                            <td class="lg:w-2/6 sm:w-1/2 w-2/3 px-2 sm:px-8 py-3 font-bold capitalize text-logogreen">
                                <a href="{{ route('article.show',$article->id) }}">{{ $article->title }}</a>
                            </td>
                            <td class="md:px-2 lg:px-8 py-3 hidden sm:table-cell capitalize">
                                {{ $article->username }}
                            </td>
                            <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell">
                                {{ \Carbon\Carbon::parse($article->created_at)->format('d/m/Y')}}
                            </td>
                            <td class="md:px-2 lg:pl-8 py-3 pr-8 flex items-center justify-end space-x-4">

                            {{-- checking if the article is already in favourite ist and the display the right button accordingly --}}
                            @php

                                if(in_array("$article->id", $userFav))
                                {
                                    echo '<button class="removeFromFavourites removeFromFav'.$article->id.'" data-id='.$article->id.'><i class="fas fa-heart" title="remove from favourites"></i></i></button>';
                                } else 
                                {
                                    echo '<button class="saveFavourite saveToFav'.$article->id.'" data-id='.$article->id.'><i class="far fa-heart" title="add to favourites"></i></button>';
                                }
                            @endphp
                                

                            {{-- @can('delete articles')
                                <button class="deleteRecord" data-id="{{ $article->id }}" data-title="{{ $article->title }}" ><i class="fas fa-trash-alt" title="delete"></i></button>
                            @endcan --}}
                
                            @can('edit articles')
                                <a href="{{ route('article.edit',$article->id) }}"><i class="fas fa-pen" title="edit"></i></a>
                            @endcan
                            </td>
                            <td class="hidden sm:table-cell md:px-2 lg:pl-8 py-3 pr-8 items-center justify-end space-x-4">
                                {{ $article->status_comment }}
                            </td>
                        </tr>
                            @empty
                                <p>There are no "inactive" articles!!</p>        
                            @endforelse
                </table>
                <div class="mt-2">
                    {{ $articles->links() }}
                </div>
        </div>
    
<script>

    //delete selected article
$(".deleteRecord").click(function()
{    
    var title = $(this).data("title");
  
    //check if you really want to delete
      if(!confirm("delete "+title)){return false};
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
                    
    
        $.ajax(
        {
            url: "articles/"+id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
                //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                }).showToast();

                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $("."+id).remove(); 
                } 

            }   
        });
    
}
);

//save selected article into favourites list
$(document).on('click', '.saveFavourite', function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    var isAlreadyFav = ""

    $.ajax(
    {
        url: "{{ route('addToFavourites')}}",
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success:function(response){   
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();

            //change the fav button?
            $(".saveToFav"+id).replaceWith('<button class="removeFromFavourites removeFromFav'+id+'" data-id="'+id+'"><i class="fas fa-heart" title="remove from favourites"></i></button>');

                
        },
        error:function(error){
            console.log(error)
        }
    });

});


$(document).on('click','.removeFromFavourites',function()
{
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
                    
    
        $.ajax(
        {
            url: "{{ route('removeFromFavourites')}}",
            type: 'POST',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();

            //change the fav button?
            $(".removeFromFav"+id).replaceWith('<button class="saveFavourite saveToFav'+id+'" data-id="'+id+'"><i class="far fa-heart" title="add to favourites"></i></button>');
            }
        });
    
});
    
    </script>
</x-app-layout>
