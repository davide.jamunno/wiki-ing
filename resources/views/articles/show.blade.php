
<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9">
            
            <div class="px-1 pt-1 text-sm font-medium leading-5 text-gray-900 ">{{ $article->title }}</div>
            
            <div class="flex justify-between items-center space-x-2">
                @can('edit articles')
                <a href="{{ route('article.edit',$article->id) }}" class=" nostyle"><i class="fas fa-pen" title="edit"></i></a>
                @endcan
                @php
                    if(in_array("$article->id", $userFav))
                    {
                        echo '<button class="removeFromFavourites removeFromFav'.$article->id.'" data-id='.$article->id.'><i class="fas fa-heart" title="remove from favourites"></i></i></button>';
                    } else {
                        echo '<button class="saveFavourite saveToFav'.$article->id.'" data-id='.$article->id.'><i class="far fa-heart" title="add to favourites"></i></button>';
                    }
                @endphp
                {{-- <div class="mx-3">
 
                    @can('delete articles')
                    <button class="deleteRecord" data-id="{{ $article->id }}" data-title="{{ $article->title }}" ><i class="fas fa-trash-alt" title="delete"></i></button> 
                    @if ($article->status === 1)
                    
                    <div class="show">
                        <button class="setStatusToZero"><i class="fas fa-eye-slash" title="set status to Zero"></i></button>
                    </div>
                    
                    <div id="toShow" style="display:none">
                        <form method="POST" id="formCommentStatus" action="{{ route('changeArticleComment') }}">
                            @csrf
                            <input type="text" id="status_comment" data-id="{{ $article->id }}" name="status_comment">
                            <button type="submit" class="btn btn-primary">Save comment</button>
                        </form>
                    </div>
                    @else
                    <div id="content">
                        <button data-id="{{ $article->id }}" class="setStatusToOne">save article as "active"</button>
                    </div>
        
                    @endif
                    @endcan
                </div> --}}

                <!-- Settings Dropdown -->
                @can('write articles')     
                <div class=" flex sm:items-center sm:ml-6">
                    <x-dropdown align="right" width="48">
                        <x-slot name="trigger">
                            <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                <div class="whitespace-nowrap">
                                    save into
                                </div>
                                <div class="ml-1">
                                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                            </button>
                        </x-slot>
                        
                        <x-slot name="content">
                            <ul class="nostyle">
                                {{-- creates a new reading list and save the article in it --}}
                                @can('write articles')
                                <li class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a class="open" href="#"data-artid="{{ $article->id }}">new reading list</a></li>
                               
                    
                                @endcan
                                
                                @foreach ($readinglists as $list )
                                    <li class="my-2"><a class="list hover:bg-pink-200 border-white rounded p-1 my-2" href="#" data-listid="{{ $list->id }}" data-artid="{{ $article->id }}">{{ $list->name }}</a></li>
                                @endforeach
                                
                            </ul>
                        </x-slot>
                    </x-dropdown>
                </div>
                
                @endcan
            </div>
        </div>
    </x-slot>
     {{-- modal!! --}}                         
    <div class="modal-overlay" id="overlay" >
        <div class="modal w-full max-w-md w-full mx-0 bg-white shadow-md rounded-md " id="modal">
            <div class="flex justify-between items-center bg-pink-300 rounded-t-md text-lg px-4">
                <p class="mt-3 mb-3">Create a new reading list</p>
                <button class="close text-lg"><i class="fas fa-times" title="close"></i></button>
            </div>
            <div class="modalContent sm:flex sm:justify-between p-4">
               
            </div>
        </div>
    </div>
    @push('head')
        <style>
            /* add padding to dropdown element */
            .pesce {
                padding: 1rem;
            }

            /* reset tailwind attributes */
            .content *{
                color: revert;
                list-style: revert;
                list-style-position: inside;
                text-decoration: revert;  
            }
            h1 {
                font-size: revert;
            }
            h2 {
                font-size: revert;
            }
            h3 {
                font-size: revert;
            }

            /* The Picture Modal (background) */
            .modalf {
                display: none;
                position: fixed;
                z-index: 1;
                /* padding-top: 100px;
                padding-bottom: 50px; */
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgb(3, 3, 3);
            }

            /* Modal Content */
            .modal-content {
                /* position: relative; */
                background-color: black;
                display: flex;
                align-items: center;
                height: 100%;
                justify-content: space-between;
            }

            .mySlides {
                margin: auto;
                height: 100%;
                background-color: black;
                display: flex;
                align-items: center;
                justify-content: space-between;
            }

            .imageModal {      
                max-height: 80%;
                width: auto;         
            }

            /* The Close Button */
            .closef {
                z-index: 2;
                color: white;
                position: absolute;
                top: 10px;
                right: 25px;
                font-size: 35px;
                font-weight: bold;
            }

            .closef:hover,
            .closef:focus {
                color: #999;
                text-decoration: none;
                cursor: pointer;
            }

            .mySlides {
                display: none;
            }

            .cursor {
                cursor: pointer;
            }

            /* Next & previous buttons */
            .prev,
            .next {
                cursor: pointer;
                /* position: absolute;
                top: 50%; */
                width: auto;
                padding: 16px;
                margin-top: -50px;
                color: white;
                font-weight: bold;
                font-size: 20px;
                transition: 0.6s ease;
                border-radius: 0 3px 3px 0;
                user-select: none;
                -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
                right: 0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }


        </style>
    @endpush
    @if ($message = Session::get('success'))
        <script>
            window.onload = function() {
                Toastify({
                    text: 'thanks for writing an article ;)',
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                    }).showToast();
            }; 
        </script>
    @endif

    <div class="m-3 mx-auto max-w-3xl">

        <div class="border border-gray-400 bg-white rounded-sm p-4 flex flex-col justify-between ">
            <div class="mb-8">
        
            <div class="text-gray-900 font-bold text-xl mb-2">{{ $article->title }}</div>
            <div class="content text-gray-700">{!! $article->content !!}</div>
            
        </div>

        <div class="flex items-center justify-between space-x-4">
            <div class="text-sm border border-gray-400 rounded-sm p-2">
                <p class="text-gray-900 leading-none">written by <b>{{ $article->username }}</b> on {{ \Carbon\Carbon::parse($article->articleDate)->format('d/m/Y')}} </p>
            </div>   
        </div>

    </div>
 


@if(count($uploads) > 0) 


    {{-- picture preview --}}
    <div class="pt-4 flex flex-wrap sm:grid grid-cols-3 gap-4">
    
        @foreach ($uploads as $picture)
            <div class="justify-self-center border-4 border-gray-80 h-48 w-48 cursor-pointer">
                <img class="galleryimg object-cover h-full w-full hover-shadow cursor" src="{{asset('uploads/' . $picture->name)}}" alt="{{ $picture->name }}" onclick="openModal();currentSlide({{ $loop->iteration }})"> 
            </div>
        @endforeach

    </div>

    {{-- modal lightbox --}}
    <div id="myModal" class="modalf">
        <span class="closef cursor" onclick="closeModal()">&times;</span>
        <div class="modal-content">
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
           
            @foreach ($uploads as $picture)
            <div class="mySlides">
                <img src="{{asset('uploads/' . $picture->name)}}" class="imageModal"> 
            </div>
            
            @endforeach
            
            <a class="next" onclick="plusSlides(1)">&#10095;</a>
            
            
        </div>
    </div>

<script>
    
//modal lightbox
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}
//close modalf window on clicking modal-overlay
$(".modal-content").on("click", function(e){
    if($(e.target).hasClass("modal-content") || $(e.target).hasClass("mySlides")){
        $("#myModal").hide();
    }
});

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  
  slides[slideIndex-1].style.display = "flex";
 
}
</script>
@endif
    
<script>

//save selected article into favourites list
$(document).on('click', '.saveFavourite', function()
{
    var id = $(this).data("id");
    
    var token = $("meta[name='csrf-token']").attr("content");
    
    $.ajax(
        {
            url: "{{ route('addToFavourites')}}",
            type: 'POST',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //change the fav button?
            $(".saveToFav"+id).replaceWith('<button class="removeFromFavourites removeFromFav'+id+'" data-id="'+id+'"><i class="fas fa-heart" title="remove from favourites"></i></button>');

                
        },
        error:function(error){
            console.log(error)
        }
    });

}
);

//remove from fav list
$(document).on('click','.removeFromFavourites',function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
                    
    $.ajax(
    {
        url: "{{ route('removeFromFavourites')}}",
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //change the fav button?
            $(".removeFromFav"+id).replaceWith('<button class="saveFavourite saveToFav'+id+'" data-id="'+id+'"><i class="far fa-heart" title="add to favourites"></i></button>');
        }
    });  
}
);

//delete the article    
$(".deleteRecord").click(function()
{    
    var title = $(this).data("title");
  
    //check if you really want to delete
        if(!confirm("delete "+title)){return false};
        var id = $(this).data("id");
        var token = $("meta[name='csrf-token']").attr("content");
                    
    
        $.ajax(
        {
            url: "articles/"+id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
                //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();

                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $("."+id).remove(); 
                } 

            }   
        });
    
});


//create a new reading list and saves the article into it.
$(".open").on("click", function()
{
    //closes dropdown
    $(".closeme").hide();
    //reset modal
    $('.modalContent').empty();
    var token = $("meta[name='csrf-token']").attr("content");
    var article_id = $(this).data('artid');

    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");
    
    //Modal content added with js
    $(".modalContent").append("<form method='POST' action='#' class='formReadingList mx-auto flex flex-col' ><input type='hidden' name='_token' value='"+token+"'><input class='w-md mx-auto mb-4 border rounded' type='text' id='readingListName'  name='readingList' placeholder='name'><button type='submit' class='confirm text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded w-32 mx-auto' id='confirmBtn'>Go!</button></form>");

    $(document).ready(function() 
    {
        $(".formReadingList").submit(function (event) {
            
            
            var formData = {
                "article_id": article_id,
                readingListName: $("#readingListName").val(),
                _token: $("meta[name='csrf-token']").attr("content")
            };
            
            
            $.ajax(
                {
                    type: "POST",
                    url: "{{ route('readinglist.createwitharticle') }}",
                    data: formData,
                    dataType: "json",             
                    success:function(response){
                        Toastify({
                            text: response.message,
                            offset: {
                                x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                                y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                            },
                            backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                        }).showToast();
                        $(".modal-overlay").removeClass("active");
                    }
                });
                event.preventDefault();
            });
        });
});

//comment an article when status is set to 0
$(document).ready(function() 
{
    $("#formCommentStatus").submit(function (event) {
        $("#toShow").hide();
        $('.show').html('this article has been set as "inactive"');
        var formData = {
            id: $("#status_comment").data('id'),
            status_comment: $("#status_comment").val(),
            status: 0,
            _token: $("meta[name='csrf-token']").attr("content")
        };
        
        $.ajax(
        {
            type: "POST",
            url: "/articles/change-comment",
            data: formData,
            dataType: "json",             
            success:function(response){
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
            }
        });
        event.preventDefault();
    });
});

//set the articel to active
$(".setStatusToOne").click(function()
{   
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax(
        {
            url: '/articles/change-status-active',
            type: 'POST',
            dataType: "json", 
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
                //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();

                $("#content").hide();
                $(".alternative1").show();
                

            }   
        }
    );
});

//Modal window open
$(".open").on("click", function(){
    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");

    //close modal when clicking outside the window?

});

//close modal window
$(".close").on("click", function(){
    $(".modal-overlay").removeClass("active");
    
});
//close modal window on clicking modal-overlay
$(".modal-overlay").on("click", function(e){
    if($(e.target).hasClass("modal-overlay")){
        $(".modal-overlay").removeClass("active");
    }
});

//save article into an existing list
$(".list").on("click", function(){
    var listid = $(this).data("listid");
    var artid = $(this).data("artid");
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax(
    {
        url: '{{ route("readinglist.save-article") }}',
        type: 'POST',
        dataType: "json", 
        data: {
            "listid": listid,
            "artid": artid,
            "_token": token,

        },
        success:function(response){
            //message via Toaster
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            $(".modal-overlay").removeClass("active");
                
        }   
    })
});

    
    </script>
</x-app-layout>