<x-app-layout>
    @push('head')
    <style>
    input:checked + label {
        border-color: black;
        box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
    }
    </style>
    @endpush
    <x-slot name="header">
        <div class="relative h-20 sm:h-9">
            <div class="absolute inset-x-0 -bottom-3 flex text:md sm:text-lg text-gray-600 capitalize">
                <a  class=" p-1
                @if ($cat == '') 
                    bg-pink-200 border-t rounded-t
                @endif
                " href="{{ route('search') }}">All</a>
                @foreach ($categories as $category)
                    
                    <a data-cat="{{ $category->id }}" class=" tabs p-1
                        @if ($cat == $category->id) 
                            bg-pink-200 border-t rounded-t
                        @endif
                        " href="{{ route('searchCategory', ['cat' => $category->id]) }}">{{ $category->name }}</a>
                       
                    
                @endforeach
            </div>
            <!-- Search  -->
            <div class=" sm:absolute sm:inset-y-0 sm:right-0 max-w-sm">
                <form  action="{{ route('search') }}" method="GET" role="search">
                    {{-- do I need token with get??? --}}
                    {{-- @csrf --}}
                    <div class="flex ">
                       
                        {{-- search field --}}
                        <input type="text" class="border-logogreen border-2 w-full mr-2 rounded ml-1" placeholder="Search for..." name="q">
                        <button class="">
                            <span class="w-auto flex justify-end items-center text-grey  hover:text-grey-darkest border-gray">
                                  <i class="fas fa-search"></i>
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    {{-- </div> --}}
    </x-slot>

    <div class="max-w-7xl mx-auto pb-3 px-4 sm:px-6 lg:px-8">
        <table class="text-left w-full">
            <div class="border-b-4 border-pink-200">
               
            </div>
         
            <tbody>
                @forelse ($all_articles as $article)
                    <tr class="{{ $article->id }}  bg-white border-b border-gray-200" >
                        <td class="lg:w-2/6 sm:w-1/2 w-2/3 px-2 sm:px-8 py-3 font-bold capitalize text-logogreen">
                             <a href="{{ route('article.show', $article->id) }}">{{ $article->title }}</a>
                        </td>
                        <td class="md:px-2 lg:px-8 py-3 hidden sm:table-cell capitalize">
                             {{ $article->username }}
                        </td>
                        <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell capitalize">
                             {{ $article->category }}
                        </td>
                        <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell">
                             {{ \Carbon\Carbon::parse($article->articleDate)->format('d/m/Y')}}
                        </td>
                        <td class="md:px-2 lg:pl-8 py-3 pr-8 flex items-center justify-end space-x-4">
                            @csrf
                             {{-- checking if the article is already in favourite list and the display the right button accordingly --}}
                            @php
                                if(in_array("$article->id", $userFav))
                                {
                                    echo '<button class="removeFromFavourites removeFromFav'.$article->id.'" data-id='.$article->id.'><i class="fas fa-heart" title="remove from favourites"></i></i></button>';
                                } else {
                                     echo '<button class="saveFavourite saveToFav'.$article->id.'" data-id='.$article->id.'><i class="far fa-heart" title="add to favourites"></i></button>';
                                }
                            @endphp
                            @can('edit articles')
                            <a href="{{ route('article.edit',$article->id) }}"><i class="fas fa-pen" title="edit"></i></a>
                            @endcan

                            <!-- Settings Dropdown -->
                            @can('write articles')     
                            <div class="flex sm:items-center sm:ml-6">
                                <x-dropdown align="right" width="48">
                                    <x-slot name="trigger">
                                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                            <div class="whitespace-nowrap">
                                                add to
                                            </div>
                                            <div class="ml-1">
                                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                                </svg>
                                            </div>
                                        </button>
                                    </x-slot>
                                    
                                    <x-slot name="content">
                                        <ul class="p-2">
                                            {{-- creates a new reading list and save the article in it --}}
                                            @can('write articles')
                                            <li class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a class="open" href="#" data-artid="{{ $article->id }}">new reading list</a></li>
                                            
                                            @endcan
                                            
                                            @foreach ($readinglists as $list )
                                                <li class="my-2"><a class="list hover:bg-pink-200 border-white rounded p-1 my-2" href="#" data-listid="{{ $list->id }}" data-artid="{{ $article->id }}">{{ $list->name }}</a></li>
                                            @endforeach
                                            
                                        </ul>
                                    </x-slot>
                                </x-dropdown>
                            </div>
                            @endcan
                        </td>
                    </tr>
                     
                @empty
                    <p>There are no articles here,  <a class="underline" href="{{ route('article.create') }}">click here </a>to write an article</p>        
                @endforelse
            </tbody>
                 
        </table>
        <div class="mt-2">
            {!! $all_articles->links() !!}
        </div>
    </div>
         
{{-- modal!! --}}
                                            
<div class="modal-overlay" id="overlay" >
    <div class="z-10 modal w-full max-w-md w-full mx-0 bg-white shadow-md rounded-md " id="modal">
        <div class="flex justify-between items-center bg-pink-300 rounded-t-md text-lg px-4">
            <h2 class="mt-3 mb-3">Create a new reading list</h2>
            <button class="close text-lg"><i class="fas fa-times" title="close"></i></button>
        </div>
        <div class="modalContent sm:flex sm:justify-between p-4">
           
        </div>
    </div>
</div>

<script>

//save selected article into favourites list
$(document).on('click', '.saveFavourite', function()
{
    var id = $(this).data("id");
    
    var token = $("meta[name='csrf-token']").attr("content");
    
    $.ajax(
        {
            url: "{{ route('addToFavourites')}}",
            type: 'POST',
            data: {
                "id": id,
                "_token": token,
            },
            success:function(response){
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //change the fav button?
            $(".saveToFav"+id).replaceWith('<button class="removeFromFavourites removeFromFav'+id+'" data-id="'+id+'"><i class="fas fa-heart" title="remove from favourites"></i></button>');

                
        },
        error:function(error){
            console.log(error)
        }
    });

}
);


//remove from fav list
$(document).on('click','.removeFromFavourites',function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
                    
    $.ajax(
    {
        url: "{{ route('removeFromFavourites')}}",
        type: 'POST',
        data: {
            "id": id,
            "_token": token,
        },
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //change the fav button?
            $(".removeFromFav"+id).replaceWith('<button class="saveFavourite saveToFav'+id+'" data-id="'+id+'"><i class="far fa-heart" title="add to favourites"></i></button>');
        }
    });  
}
);

//create a new reading list and saves the article into it.
$(".open").on("click", function()
{
    //closes dropdown
    $(".closeme").hide();
    //reset modal
    $('.modalContent').empty();
    var token = $("meta[name='csrf-token']").attr("content");
    var article_id = $(this).data('artid');

    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");
    
    //Modal content added with js
    $(".modalContent").append("<form method='POST' action='#' class='formReadingList mx-auto flex flex-col ' ><input type='hidden' name='_token' value='"+token+"'><input class='w-md mx-auto mb-4 border rounded' type='text' id='readingListName'  name='readingList' placeholder='name'><button type='submit' class='confirm text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded w-32 mx-auto' id='confirmBtn'>Go!</button></form>");

    $(document).ready(function() 
    {
        $(".formReadingList").submit(function (event) {
            
            
            var formData = {
                "article_id": article_id,
                readingListName: $("#readingListName").val(),
                _token: $("meta[name='csrf-token']").attr("content")
            };
            
            
            $.ajax(
                {
                    type: "POST",
                    url: "{{ route('readinglist.createwitharticle') }}",
                    data: formData,
                    dataType: "json",             
                    success:function(response){
                        Toastify({
                            text: response.message,
                            offset: {
                                x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                                y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                            },
                            backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                        }).showToast();
                        $(".modal-overlay").removeClass("active");
                    }
                });
                event.preventDefault();
            });
        });
});
        


//close modal window
$(".close").on("click", function(e){
    $(".modal-overlay").removeClass("active");
    
});
//close modal window on clicking modal-overlay
$(".modal-overlay").on("click", function(e){
    if($(e.target).hasClass("modal-overlay")){
        $(".modal-overlay").removeClass("active");
    }
});

//save article into an existing list
$(".list").on("click", function(){
    var listid = $(this).data("listid");
    var artid = $(this).data("artid");
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax(
    {
        url: '{{ route("readinglist.save-article") }}',
        type: 'POST',
        dataType: "json", 
        data: {
            "listid": listid,
            "artid": artid,
            "_token": token,

        },
        success:function(response){
            //message via Toaster
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            $(".modal-overlay").removeClass("active");
                
        }   
    })
});
    
//switching categories with ajax
/* $('.tabs').click(function(e){
    e.preventDefault();
    var cat = $(this).data('cat');

    $.ajax({
        type: 'GET',
        url: '{{ route("searchCategory") }}',
        data: {
            "cat": cat
        },
        success: function(response){
            console.log('funziona!');
            //manipulate DOM
            /* console.log(response.articles.data[0]);
            console.log(response.articles.data);
            var output = '';
            for(var count = 0; count < response.articles.data.length; count++)
            {
                output += '<tr>';
                output += '<td>' + response.articles.data[count].title + '</td>';
                output += '<td>' + response.articles.data[count].username +'</td>';
                output += '<td>' + response.articles.data[count].category +'</td>';
                output += '<td>' + response.articles.data[count].articleDate +'</td>';
                output += '</tr>';

            } 
            $('tbody').html(output);
           

            
            
        },
        error: function(xhr){
            console.log('pene');
            console.log(xhr.responseText);
        }
    });
}); */

</script>
    
</x-app-layout>