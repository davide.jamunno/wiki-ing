<x-app-layout>
    <x-slot name="header">
      <div class="flex items-center flex-start h-9">
        <x-nav-link :href="route('favourites')" :active="request()->routeIs('favourites')">
          {{ __('Favourites') }}
        </x-nav-link>
      </div>
    </x-slot>
   
    <div class="max-w-7xl mx-auto pb-3 px-4 sm:px-6 lg:px-8">
        <table class="text-left w-full">
          
            <div class="border-b-4 border-pink-200"></div>
            {{-- <tr class="text-lg font-bold bg-white">
              <th class="px-8 py-3">
                <span>Title</span>
              </th>
              <th class="px-8 py-3 hidden sm:table-cell">
                <span>Author</span>
              </th>
              <th class="px-8 py-3 hidden sm:table-cell">
                            <span>Category</span>
                        </th>
              <th class="px-8 py-3 hidden sm:table-cell">
                <span>Created on</span>
              </th>
              <th class="px-8 py-3">
                <span>Actions</span>
              </th>
              <th class="px-8 py-3 hidden sm:table-cell">
                <span>Info</span>
              </th>
            </tr> --}}
          

          <tbody>
            @forelse ($favourites as $article)
                @if ($article->articleStatus === 1)

                  <tr class="{{ $article->article_id }} bg-white border-b border-gray-200">
                    <td class="g:w-2/6 sm:w-1/2 w-2/3 px-2 sm:px-8 py-3 font-bold capitalize text-logogreen">
                      <a href="{{ 'articles/'.$article->article_id }}">{{ $article->title }}</a>
                    </td>
                    <td class="md:px-2 lg:px-8 py-3 hidden sm:table-cell capitalize">
                      {{ $article->username }}
                    </td>
                    <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell capitalize">
                      {{ $article->category }}
                    </td>
                    <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell">
                      {{ \Carbon\Carbon::parse($article->articleDate)->format('d/m/Y')}}
                    </td>
                    <td class="md:px-2 lg:pl-8 py-3 pr-8 flex items-center justify-end space-x-4">
                      <button class="RemoveFromFavourites mr-2" data-id="{{ $article->article_id }}">
                        <i class="fas fa-heart-broken" title="remove from favourites">
                        </i>
                      </button>
                      <!-- Settings Dropdown -->
                      @can('write articles')     
                      <div class="flex sm:items-center sm:ml-6">
                          <x-dropdown align="right" width="48">
                              <x-slot name="trigger">
                                  <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                      <div class="whitespace-nowrap">
                                          add to
                                      </div>
                                      <div class="ml-1">
                                          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                              <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                          </svg>
                                      </div>
                                  </button>
                              </x-slot>
                              
                              <x-slot name="content">
                                  <ul class="p-2">
                                      {{-- creates a new reading list and save the article in it --}}
                                      @can('write articles')
                                      <li class="{{-- p-1 bg-logogreen hover:bg-pink-200 border rounded --}} text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a class="open" href="#" data-artid="{{ $article->article_id }}">new reading list</a></li>
                                      
                                      @endcan
                                      
                                      @foreach ($readinglists as $list )
                                          <li class="my-2"><a class="list hover:bg-pink-200 border-white rounded p-1 my-2" href="#" data-listid="{{ $list->id }}" data-artid="{{ $article->article_id }}">{{ $list->name }}</a></li>
                                      @endforeach
                                      
                                  </ul>
                              </x-slot>
                          </x-dropdown>
                      </div>
                      @endcan
                    </td>
                    {{-- <td class="px-8 py-2 hidden sm:table-cell">
                        
                    </td> --}}
                  </tr>
                @else
                <tr title="this article is inactive, you are not allowed to read it" class="{{ $article->article_id }} bg-white">
                  <td class="lg:w-2/6 sm:w-1/2 w-2/3 px-2 sm:px-8 py-3 font-bold capitalize text-logogreen">
                    <s><a href="{{ 'articles/'.$article->article_id }}">{{ $article->title }}</a></s>
                    
                  </td>
                  <td class="md:px-2 lg:px-8 py-3 hidden sm:table-cell capitalize">
                    <s>{{ $article->username }}</s>
                  </td>
                  <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell capitalize">
                    <s>{{ $article->category }}</s>
                  </td>
                  <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell">
                    <s>{{ \Carbon\Carbon::parse($article->articleDate)->format('d/m/Y')}}</s>
                  </td>
                  <td class="px-8 py-2">
                    <button class="RemoveFromFavourites" data-id="{{ $article->article_id }}">
                      <i class="fas fa-heart-broken" title="remove from favourites">
                      </i>
                    </button>
                </tr>
                <tr class="bg-white border-b border-gray-200">
                    <td class="w-full px-2 sm:px-8 py-3">
                    {{ $article->status_comment }}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @endif
            @empty
                <p>You have no favouirites articles yet,  <a class="underline" href="{{ route('search') }}">click here </a>to look for favourites articles</p>        
            @endforelse
          </tbody>     
          
            
          
        </table>

        <div class="mt-2">
          {!! $favourites->links() !!}
        </div>
    
    </div>
{{-- modal!! --}}
<div class="modal-overlay" id="overlay" >
  <div class="modal w-full max-w-md w-full mx-0 bg-white shadow-md rounded-md " id="modal">
      <div class="flex justify-between items-center bg-pink-300 rounded-t-md text-lg px-4">
          <h2 class="mt-3 mb-3">Create a new reading list</h2>
          <button class="close text-lg"><i class="fas fa-times" title="remove"></i></button>
      </div>
      <div class="modalContent sm:flex sm:justify-between p-4">
         
      </div>
  </div>
</div>

                                     

<script>
       
$(".RemoveFromFavourites").click(function()
{
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
            url: "{{ route('removeFromFavourites')}}",
            type: 'POST',
            data: {
                      "id": id,
                      "_token": token,  
            },
            success:function(response){
              //Delete from DOM 
              $("."+id).fadeOut();
              //success message via Toast
              Toastify({
                  text: response.message,
                  offset: {
                      x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                      y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                  },
                  backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
              }).showToast();
                
            }
        });
    
});

//create a new reading list and saves the article into it.
$(".open").on("click", function()
{
    //closes dropdown
    $(".closeme").hide();
    //reset modal
    $('.modalContent').empty();
    var token = $("meta[name='csrf-token']").attr("content");
    var article_id = $(this).data('artid');

    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");
    
    //Modal content added with js
    $(".modalContent").append("<form method='POST' action='#' class='formReadingList mx-auto flex flex-col' ><input type='hidden' name='_token' value='"+token+"'><input class='w-md mx-auto mb-4 border rounded' type='text' id='readingListName'  name='readingList' placeholder='name'><button type='submit' class='confirm text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded w-32 mx-auto' id='confirmBtn'>Go!</button></form>");

    $(document).ready(function() 
    {
        $(".formReadingList").submit(function (event) {
            
            
            var formData = {
                "article_id": article_id,
                readingListName: $("#readingListName").val(),
                _token: $("meta[name='csrf-token']").attr("content")
            };
            
            
            $.ajax(
                {
                    type: "POST",
                    url: "{{ route('readinglist.createwitharticle') }}",
                    data: formData,
                    dataType: "json",             
                    success:function(response){
                        Toastify({
                            text: response.message,
                            offset: {
                                x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                                y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                            },
                            backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                        }).showToast();
                        $(".modal-overlay").removeClass("active");
                    }
                });
                event.preventDefault();
            });
        });
});
        


//close modal window
$(".close").on("click", function(){
    $(".modal-overlay").removeClass("active");
    
});
//close modal window on clicking modal-overlay
$(".modal-overlay").on("click", function(e){
    if($(e.target).hasClass("modal-overlay")){
        $(".modal-overlay").removeClass("active");
    }
});

//save article into an existing list
$(".list").on("click", function(event){
  event.preventDefault(event);
  
    var listid = $(this).data("listid");
    var artid = $(this).data("artid");
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajax(
    {
        url: '{{ route("readinglist.save-article") }}',
        type: 'POST',
        dataType: "json", 
        data: {
            "listid": listid,
            "artid": artid,
            "_token": token,

        },
        success:function(response){
            //message via Toaster
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            $(".modal-overlay").removeClass("active");
                
        }
    })
});
    
    
</script>
</x-app-layout>

