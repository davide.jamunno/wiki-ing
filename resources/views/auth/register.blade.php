<x-app-layout>
    
    <x-slot name="header">

        <div class="flex h-9 items-center">
            <div class=" capitalize my-auto text-sm font-medium leading-5 text-gray-900 ">Create User</div>
            
        </div>
        
    </x-slot>
    <div class="max-w-md space-y-5 w-full mx-auto bg-white shadow-md rounded-b-md px-6 py-4 border-t-4 border-pink-200">
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="username" :value="__('Username')" />

                <x-input id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username')" required autofocus />
            </div>


            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

            <div class="flex items-center mt-4">
                

                <button type="submit" class="mx-auto newPic text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded">
                    Go!
                </button>
            </div>
        </form>
    </div>
</x-app-layout>
