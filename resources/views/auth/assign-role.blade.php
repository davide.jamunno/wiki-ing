<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9">
            <x-nav-link :href="route('assign-role')" :active="request()->routeIs('assign-role')">
                {{ __('Assign role') }}
            </x-nav-link>
        </div>
    </x-slot>


    <div class="max-w-md space-y-5 w-full mx-auto bg-white shadow-md rounded-b-md px-6 py-4 border-t-4 border-pink-200">
                    <form class="flex flex-col" method="POST" action="{{ route('assign-role') }}">
                        @csrf

                        
                        {{-- select user--}}

                        <div class="m-auto mb-2">
                            <label class="block text-gray-500 font-bold" for="model_id">The last created user is: {{ $lastUserName[0]->username }}</label>
                        </div>
                        <div class="m-auto mb-12">
                            <select name="model_id" id="model_id" class="rounded-lg text-gray-700">
                                @foreach ($users as $user)
                                <option value="{{ $user->id }}" 
                                    @php //add "selcted" by the last registered user if
                                    if ($user->username === $lastUserName[0]->username) {
                                    echo('selected');
                                    }
                                    @endphp
                                    >{{ $user->username }}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- select role --}}
                        <div class="m-auto mb-2">
                            <label class="text-gray-500 font-bold mb-1" for="model_id">Select a role:</label>
                        </div>
                        
                        <div class="mb-12 m-auto">
                            <div class="flex">
                                <div class="flex">
                                    @foreach ($roles as $role)
                                    <div class="flex items-center">
                                        <input class="mr-2 leading-tight" type="radio" value="{{ $role->name }}" name="role_name">
                                        <label class="mr-4 text-gray-500 font-bold" for="{{ $role->name }}">{{ $role->name }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="m-auto mb-2">
                            <label class="text-gray-500 font-bold mb-1" for="model_id">Select a work field:</label>
                        </div>
                        <div class="justify-between m-auto mb-12 flex">

                            @foreach ($categories as $category)
                            <div class="flex items-center">
                                <input class="mr-2 leading-tight" type="radio" name="category_id" value="{{ $category->id }}">
                                <label class="mr-4 text-gray-500 font-bold" for="{{ $category->name }}">{{ $category->name }}</label>
                            </div>
                            @endforeach

                        </div>

                        <div class="mb-10 m-auto">
                            <div class="w-full">
                                
                                <button  type="submit" class="w-32 text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded">
                                    Go!
                                </button>
                            </div>
                        </div>

                            
                        
                    </form>
    </div>
</x-app-layout>
    
