<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9">
            <div class="hidden sm:flex  justify-between items-center">
                @can('manage users')
                    <x-nav-link :href="route('users')" :active="request()->routeIs('users')">
                        {{ __('Manage users') }}
                    </x-nav-link>
                    @endcan
                @can('delete articles')
                    <x-nav-link :href="route('statusZero')" :active="request()->routeIs('statusZero')">
                        {{ __('Inactive articles') }}
                    </x-nav-link>
                @endcan

                @can('create categories')
                    <x-nav-link :href="route('categories')" :active="request()->routeIs('categories')">
                        {{ __('Categories') }}
                    </x-nav-link>
                @endcan
                
            </div>
            <div class="flex content-between">
                <button class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a href="{{ route('category.create') }}">create category</a></button>
            </div>
        </div>
    </x-slot>

    <div class="sm:flex m-auto">
        
        @foreach ($categories as $category)
        <div class="bg-pink-300 w-36 border rounded-md p-4 m-4">
            <div class="mb-8">
                
                <div class="capitalize text-gray-900 font-bold text-xl mb-2">{{ $category->name}}</div>
                
            </div>
            
            
            
        </div>
        @endforeach
    </div> 
    
</x-app-layout>