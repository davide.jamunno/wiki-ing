<x-app-layout>
    <x-slot name="header">
        <div class="flex items-center h-9">
            @can('manage users')
                <x-nav-link :href="route('users')" :active="request()->routeIs('users')">
                    {{ __('Manage users') }}
                </x-nav-link>
                @endcan
            @can('delete articles')
                <x-nav-link :href="route('statusZero')" :active="request()->routeIs('statusZero')">
                    {{ __('Inactive articles') }}
                </x-nav-link>
            @endcan

            @can('create categories')
                <x-nav-link :href="route('categories')" :active="request()->routeIs('categories')">
                    {{ __('Categories') }}
                </x-nav-link>
            @endcan
            
        </div>
    </x-slot>

    
<form class="w-full max-w-sm" action="{{ route('category.store') }}" method="post">

    {{-- verify that the token in the request input matches the token stored in the session. When these two tokens match, we know that the authenticated user is the one initiating the request. --}}
    @csrf

    
    <div class="md:flex md:items-center mb-6 py-2">
        <div class="md:w-1/3">
            <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" for="title">name</label>
        </div>
        <div class="md:w-2/3">
            <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="text" class="form-control" name="name" placeholder="write the category name here">
        </div>
    </div>


    

    <div class="md:flex md:items-center">
        <div class="md:w-1/3"></div>
        <div class="md:w-2/3">
            <button class="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>

    
</form>


</x-app-layout>