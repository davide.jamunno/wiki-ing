<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9">
            <div class="flex justify-between items-center">
                <x-nav-link :href="route('readinglists')" :active="request()->routeIs('readinglists')">
                    {{ __('my reading lists') }}
                </x-nav-link>
                <x-nav-link :href="route('readinglists.shared')" :active="request()->routeIs('readinglists.shared')">
                    {{ __('shared') }}
                </x-nav-link>
            </div>
        </div>
    </x-slot>
   
@foreach ($readinglists as $list)
    <div class="list{{ $list->id }} w-full max-w-md w-full mx-auto bg-white shadow-md rounded-md px-6 py-4 my-6 ">
        <div class="sm:flex sm:justify-between">
            <div class="capitalize flex items-center">
                <h2 class="font-bold">{{ $list->name }}</h2>
            </div>
            <div >
                <h2>{{ $list->username }}</h2>
            </div>
            
        </div>
        <div class="card-content  my-4"">
            @foreach ($list->articles as $article )
                <div class="article{{ $article->id }} flex justify-between items-center py-2">
                    <p><a class="hover:text-logogreen" href="{{ route('article.show', $article->id) }}"><i class="far fa-file-alt hover:text-logogreen"></i> {{ $article->title }}</a></p>
                    
                </div>
            @endforeach
        </div>
        <div class="card-comment my-3">
            <p class="font-bold">This list was shared with you with this comment: </p>
            {{ $list->comment }}
        </div>
        <div class="flex justify-end items-center">
            {{-- <div class="flex">
                <a href="#" class="deleteList" data-name="{{ $list->name }}" data-id="{{ $list->id }}" title="delete this list"><i class="far fa-trash-alt"></i></a>
                <button class="open" data-id="{{$list->id}}" data-users="{{ $users }}">share</button>
            </div> --}}
            {{-- checbox read it! --}}
            <div class="flex">
                <span>I've read the list!</span>
                <div class="relative inline-block  ml-2 mr-2 align-middle select-none transition duration-200 ease-in">
                    <input data-list_id="{{$list->id}}" class="toggle-class toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer" name="toggle" type="checkbox"  data-toggle="toggle" data-on="Active" data-off="InActive" {{ $list->read ? 'checked' : '' }}>
                    <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>
                    
                </div>
            </div>
        </div>
        
    </div>
    
@endforeach
<div class="px-6">
    {!! $readinglists->links() !!}
</div>

<!-- MODAL -->
<div class="modal-overlay" id="overlay" >
    <div class="modal" id="modal">
        <h2>share reading list with:</h2>
        <form method="POST" action="#">
            @csrf
            
            <select name="users" id="users">  
            </select>

            <input type="text" name="comment" id="comment">
            <button type="submit" class="confirm" id="confirmBtn">Confirm</button>
        </form>
        <button class="close">Close</button>
    </div>
</div>

<script>
$(".deleteList").click(function(event)
{    
    var listName = $(this).data("name");
    
    //check if you really want to delete
    if(!confirm("delete "+listName)){return false};
    var token = $("meta[name='csrf-token']").attr("content");
    var listId = $(this).data("id");
                    
    
        $.ajax(
        {
            url: "readinglist/"+listId,
            type: 'DELETE',
            data: {
                "id": listId,
                "_token": token,
            },
            success:function(response){
                //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();

                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $(".list"+listId).remove(); 
                } 

            }   
        });
        event.preventDefault();
    
}
);
//remove article from the readinglist
$('.removeFromList').on('click', function(event)
{
    var artid = $(this).data("artid");
    var listid = $(this).data("listid");
    var token = $("meta[name='csrf-token']").attr("content");
                    
    $.ajax(
    {
        url: "{{ route('removeFromList')}}",
        type: 'POST',
        data: {
            "artid": artid,
            "listid":listid,
            "_token": token,
        },
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //change the fav button?
            $(".article"+artid).remove();
        }
    });  
    event.preventDefault();
});

//share list with other user in a Modal window!!
$(".open").on("click", function()
{
    var users = $(this).data('users');
    var list_id = $(this).data('id');
    var token = $("meta[name='csrf-token']").attr("content");
    
    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");

    //close modal when clicking outside the window??

    users.forEach(user => {
        $('#users').append("<option value="+user['id']+">"+user['username']+"</option>")
    });
    /* $('#modalUsername').html(username) */
  
    $(document).ready(function () {
        $("form").submit(function (event) {
            var formData = {
                "user_id": $("#users").val(),
                "comment": $("#comment").val(), 
                "list_id": list_id,
                "_token": token
            };

            $.ajax({
                type: "POST",
                url: "{{ route('shareList') }}",
                data: formData,
                dataType: "json",
                encode: true,
                success:function(response){
                //message via Toaster
                    Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                        },
                        backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                    }).showToast();
                    //close modal when assign a category
                    $(".modal-overlay").removeClass("active");
                }  
            });  
            event.preventDefault();
        });
    });
});


$(".close").on("click", function(){
    $(".modal-overlay").removeClass("active");
    
});

//set the list as read
$(function() {
    $('.toggle-class').change(function() {
        
        var read = $(this).prop('checked') == true ? 1 : 0; 
        var list_id = $(this).data('list_id'); 
         
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('readinglists.readIt') }}',
            data: {
                'read': read, 
                'list_id': list_id
            },
            success:function(response){
           

            
        }
        });
    })
});


</script>
</x-app-layout>
