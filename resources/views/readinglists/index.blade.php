<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center h-9">
            <div class="flex justify-between items-center">
                <x-nav-link :href="route('readinglists')" :active="request()->routeIs('readinglists')">
                    {{ __('my reading lists') }}
                </x-nav-link>
                
                <x-nav-link :href="route('readinglists.shared')" :active="request()->routeIs('readinglists.shared')">
                    {{ __('shared') }}
                </x-nav-link>
                
            </div>
            <button class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a href="{{ route('readinglist.create') }}">new reading list</a></button>
        </div>
    </x-slot>
    
    <div class="w-full mx-auto bg-gray-100 px-6 my-6">    
            @foreach ($readinglists as $list)    
                    <div class="list{{ $list->id }} max-w-md w-full mx-auto bg-white shadow-md rounded-md px-6 py-4 my-6 ">
                        <div class="flex justify-between">
                            <div class="capitalize flex items-center">
                                <h2 class="font-bold">{{ $list->name }}</h2>
                            </div>
                            <div class="pt-3">
                                <a href="#" class="deleteList pr-3" data-name="{{ $list->name }}" data-id="{{ $list->id }}" title="delete this list"><i class="far fa-trash-alt fa-lg text-gray-600 hover:text-logogreen"></i></a>
                                <button class="open" data-id="{{$list->id}}" data-users="{{ $users }}" title="share"><i class="fas fa-share fa-lg hover:text-logogreen text-gray-600"></i></button>
                            </div>
                            {{-- <div >
                                <h2 class="text-gray-900">by {{ $list->username }}</h2>
                            </div> --}}
                            
                        </div>
                        <div class="card-content my-4">
                            @foreach ($list->articles as $article )
                            <div class="article{{ $article->id }}list{{ $list->id }} flex justify-between items-center py-2">
                                <p><a class="hover:text-logogreen" href="{{ route('article.show', $article->id) }}"><i class="far fa-file-alt hover:text-logogreen"></i> {{ $article->title }}</a></p>
                                <a href="#" class="removeFromList" data-artid="{{ $article->id }}" data-listid="{{ $list->id }}" title="remove from list"><i class="fas fa-minus-circle text-gray-500 hover:text-red-600"></i></a>
                            </div>
                            @endforeach
                        </div>
                        <h2 class="font-bold mt-2">shared with:</h2>
                        @if(!$list->users->isEmpty())
                        <!-- shared with user-->
                        
                        <div id="sharedwith" class="flex flex-wrap max-w-sm sharedWith{{ $list->id }}">
                        @foreach($list->users as $user) 
                            
                            <div class="card{{ $user->username }}{{ $list->id }} capitalize text-gray-900 flex justify-between items-center content-center  max-w-min  mx-1 bg-gray-100 border rounded-md px-2 py-2 my-1">
                                    
                                <p class="mr-2">{{ $user->username}}</p>
                                
                                <div class="flex items-center">
                                    
                                    @if ($user->pivot->read === 1) 
                                    <i title="i've read the list!" class="mr-2 text-logogreen fas fa-check-double"></i>
                                    @else
                                    <div></div>
                                    @endif

                                    <!-- stop sharing with user-->
                                    <button class="stopSharing" data-user_id="{{ $user->id }}" data-username="{{ $user->username }}" data-list_id="{{ $list->id }}" title="stop sharing"><i class="fas fa-minus-circle text-gray-500 hover:text-red-600"></i></button>
                                </div>
                            </div>
                            
                        @endforeach
                        </div>
                                
                        
                        @else
                        <div class="max-w-sm sharedWith{{ $list->id }}">
                            <div class="toCancelShared{{ $list->id }} max-w-max w-full mx-auto bg-gray-100 border rounded-md px-2 py-2 my-6 ">
                                this list is not shared yet
                            </div>
                        </div>
                        @endif   
                    </div>
            @endforeach
            
    </div>
    <div class="px-6">
        {!! $readinglists->links() !!}
    </div>
                
                <!-- MODAL -->
                <div class="modal-overlay" id="overlay" >
                    <div class="modal w-full max-w-md w-full mx-0 bg-white shadow-md rounded-md" id="modal">
                        <div class="flex justify-between items-center bg-pink-300 rounded-t-md px-4">
                            <p class="mt-3 mb-3 text-lg">Share reading list with:</p>
                            <button class="close text-lg"><i class="fas fa-times" title="remove"></i></button>
                        </div>
                        <div class="modalContent  px-4">
                        </div>
                    </div>
                </div>

<script>


$(".deleteList").click(function(event)
{    
    var listName = $(this).data("name");
    
    //check if you really want to delete
    if(!confirm("delete "+listName)){return false};
    var token = $("meta[name='csrf-token']").attr("content");
    var listId = $(this).data("id");
                    
    
        $.ajax(
        {
            url: "readinglist/"+listId,
            type: 'DELETE',
            data: {
                "id": listId,
                "_token": token,
            },
            success:function(response){
                //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();

                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $(".list"+listId).fadeOut(); 
                    $(".toCancelShared"+listId).fadeOut();
                } 

            },
            error:function(response) { 
                //message via Toaster
                Toastify({
                    text: 'sorry, this list cannot be deleted, try to remove articles and/or shared users from it first!',
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
            }     
        });
        event.preventDefault();
    
}
);
//remove article from list
$('.removeFromList').on('click', function(event)
{   
    if(!confirm("remove article from list? ")){return false};
    var artid = $(this).data("artid");
    var listid = $(this).data("listid");
    var token = $("meta[name='csrf-token']").attr("content");
                    
    $.ajax(
    {
        url: "{{ route('removeFromList')}}",
        type: 'POST',
        data: {
            "artid": artid,
            "listid":listid,
            "_token": token,
        },
        success:function(response){
            //success message via Toast
            Toastify({
                text: response.message,
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //delete from DOM
            $(".article"+artid+"list"+listid).fadeOut();
        }
    });  
    event.preventDefault();
});

//delete selected list from user
$(".stopSharing").click(function pene()
{    
   
    var user_id = $(this).data("user_id");
    var username = $(this).data("username");
    var list_id = $(this).data("list_id");
    var token = $("meta[name='csrf-token']").attr("content");
 
    if(!confirm("stop sharing with "+username)){return false};
    
    $.ajax(
    {
        url: "{{ route('remove-list') }}",
        type: 'POST',
        data: {
            "user_id": user_id,
            "list_id": list_id,
            "_token": token,
        },
        success:function(response){
            //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
                
                
                
                 //check if there are other children in element. if no, write something
                if ( $('.sharedWith'+list_id).children().length == 1 ) {
                    $(".card"+username+list_id).fadeOut(1000, function(){
                        $(".card"+username+list_id).remove();
                    
                    }); 
                    $(".sharedWith"+list_id).append("<div class='toCancelShared"+list_id+" max-w-sm w-full mx-auto bg-white shadow-md rounded-md px-2 py-2 my-6'>this list is not shared yet</div>");
                } else {
                     //Delete from DOM 
                    $(".card"+username+list_id).fadeOut(1000, function(){
                        $(".card"+username+list_id).remove();
                    }); 

                }
        }   
    });   
               
});


//share list with other user in a Modal window!!
$(".open").on("click", function()
{ 
    $('.modalContent').empty();
    
    var users = $(this).data('users');
    const list_id = $(this).data('id');
    var token = $("meta[name='csrf-token']").attr("content");
    
    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");

    //Modal content added with js 
    $(".modalContent").append("<form method='POST' action='#' class='flex-col flex space-y-4'><input type='hidden' name='_token' value='"+token+"'><select class='mx-auto rounded' name='users' id='users'></select><input type='text' name='comment' id='comment' placeholder='write a comment...' class='rounded w-44 mx-auto'><button type='submit' class='confirm text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded w-32 mx-auto' id='confirmBtn'>Go!</button></form>");
    
    //close modal when clicking outside the window??

    users.forEach(user => {
        $('#users').append("<option id=user"+user['id']+" data-username="+user['username']+" value="+user['id']+">"+user['username']+"</option>")
    });
    
    //share list with an user
    $(document).ready(function () {
        $("form").submit(function (event) {
            var user_id = $("#users").val();
            
            var username = $("#user"+user_id).data('username');
            var formData = {
                "user_id": $("#users").val(),
                "comment": $("#comment").val(), 
                "list_id": list_id,
                "_token": token
            };

            $.ajax({
                type: "POST",
                url: "{{ route('shareList') }}",
                data: formData,
                dataType: "json",
                encode: true,
                success:function(response){
                //message via Toaster
                    Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                        },
                        backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                    }).showToast();
                    //reset the modal?
                    $('.modalContent').empty();
                    //modify DOM just if necessary
                    if(response.success == 'true'){

                        //DOM manipulation
                        output = "";
                        output += "<div class='card"+username+list_id+" capitalize text-gray-900 flex justify-between items-center content-center  max-w-min  mx-1 bg-gray-100 border rounded-md px-2 py-2 my-1'>";

                        output += "<p class='mr-2'>"+username+"</p>";
                        
                        output += "<button class='stopSharing' ";
                        
                        var nameoffunction = "deletesharing('"+user_id+"','"+username+"','"+list_id+"')";

                        output += "onclick="+nameoffunction;
    
                        /* output += "data-user_id='"+user_id+"' data-username='"+username+"' data-list_id='"+list_id+"'"; */

                        output += "><i class='fas fa-minus-circle text-gray-500 hover:text-red-600'></i></button>";
                        
                        output += "</div>";

                        $(output).hide().appendTo(".sharedWith"+list_id).fadeIn(1000);
                        

                        $(".toCancelShared"+list_id).remove();
                    }
                    //close modal
                    $(".modal-overlay").removeClass("active");
                    $('.modalContent').empty();                
                }                 
            });  
            event.preventDefault();
        });
    });
});



$(".close").on("click", function(){
    $(".modal-overlay").removeClass("active");
    
});
//close modal window on clicking modal-overlay
$(".modal-overlay").on("click", function(e){
    if($(e.target).hasClass("modal-overlay")){
        $(".modal-overlay").removeClass("active");
    }
});

//function for inline javascript for new dom element!
function deletesharing(user_id, username, list_id, token)
{    
    /* var user_id = $(this).data("user_id");
    var username = $(this).data("username");
    var list_id = $(this).data("list_id");*/
    var token = $("meta[name='csrf-token']").attr("content"); 
 
    if(!confirm("stop sharing with "+username)){return false};
    
    $.ajax(
    {
        url: "{{ route('remove-list') }}",
        type: 'POST',
        data: {
            "user_id": user_id,
            "list_id": list_id,
            "_token": token,
        },
        success:function(response){
            //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
                
                
                
                 //check if there are other children in element. if no, write something
                if ( $('.sharedWith'+list_id).children().length == 1 ) {
                    $(".card"+username+list_id).fadeOut(1000, function(){
                        $(".card"+username+list_id).remove();
                    
                    }); 
                    $(".sharedWith"+list_id).append("<div class='toCancelShared"+list_id+" max-w-sm w-full mx-auto bg-white shadow-md rounded-md px-2 py-2 my-6'>this list is not shared yet</div>");
                } else {
                     //Delete from DOM 
                    $(".card"+username+list_id).fadeOut(1000, function(){
                        $(".card"+username+list_id).remove();
                    }); 

                }
        }   
    });   
               
};


</script>
</x-app-layout>
