<x-app-layout>
    <x-slot name="header">
        <x-nav-link :href="route('readinglist.create')" :active="request()->routeIs('readinglist.create')">
            {{ __('Reading list / Create') }}
        </x-nav-link>
    </x-slot>

    @if ($errors->any())
    <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<form class="max-w-7xl mx-auto pb-3 px-4 sm:px-6 lg:px-8" action="{{ route('readinglist.store') }}" method="post">

    {{-- verify that the token in the request input matches the token stored in the session. When these two tokens match, we know that the authenticated user is the one initiating the request. --}}
    @csrf

    <div class="max-w-7xl mx-auto pb-3 px-4 sm:px-6 lg:px-8">
        <div class="mb-6">
            <div class="">
                <label class="block text-gray-500 font-bold text-center mb-1" for="title">name</label>
            </div>
            <div class=" text-center">
                <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-2/3 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="text" class="form-control" name="name" placeholder="write the list name here">
            </div>
        </div>

        <div class="text-center">
            
            <div class="">
                <button class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded" type="submit">Save</button>
            </div>
        </div>
    </div>
    
</form>


</x-app-layout>