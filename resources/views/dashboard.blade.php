<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('About Wiki-ing') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus, eius esse, expedita commodi laboriosam odio iusto cum excepturi saepe nostrum dolorem sequi impedit reiciendis ut similique nulla ex ab laudantium quasi doloribus amet aspernatur ea perspiciatis! Reprehenderit, fuga hic nam quibusdam minima natus repellendus vitae iusto impedit eligendi veniam. Aspernatur alias, voluptatem eligendi quasi similique culpa. Sed praesentium veritatis incidunt nisi laudantium repellendus asperiores. Excepturi, in quam! Aspernatur molestias fuga voluptatem corrupti nemo placeat, porro facere doloribus aperiam vel incidunt ipsa voluptate. Sint, saepe in quisquam officiis accusantium praesentium minima? Incidunt consequatur animi doloremque dolor atque voluptate aspernatur asperiores aliquam.
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
