<x-app-layout>
    <x-slot name="header">
        <div class="sm:flex justify-between items-center sm:h-9">
            <div class="hidden sm:flex justify-between items-center">
                @can('manage users')
                    <x-nav-link :href="route('users')" :active="request()->routeIs('users')">
                        {{ __('Manage users') }}
                    </x-nav-link>
                    @endcan
                @can('delete articles')
                    <x-nav-link :href="route('statusZero')" :active="request()->routeIs('statusZero')">
                        {{ __('Inactive articles') }}
                    </x-nav-link>
                @endcan

                @can('create categories')
                    <x-nav-link :href="route('categories')" :active="request()->routeIs('categories')">
                        {{ __('Categories') }}
                    </x-nav-link>
                @endcan
                
            </div>
            <div class="flex content-between">
                <button class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a href="{{ route('register') }}">create user</a></button>
                <button class="text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded"><a href="{{ route('assign-role') }}">assign role</a></button>
            </div>
        </div>
    </x-slot>

    
    
    <div class="max-w-7xl mx-auto pb-3 px-4 sm:px-6 lg:px-8">
        <table class="text-left w-full">
            <div class="border-b-4 border-pink-200"></div>
            
            <tbody>              
            @foreach ($users as $user)
                <tr class="bg-white border-b border-gray-200">
                    {{-- username --}}
                    <td class="lg:w-2/6 sm:w-1/2 w-1/3 px-2 sm:px-8 py-3 font-bold capitalize text-logogreen">
                        <a href="{{ 'users/'.$user->id }}">{{ $user->username }}</a>
                    </td>
                    {{-- date --}}
                    <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell">
                        {{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y')}}
                    </td>
                    {{-- status --}}
                    <td class="md:px-2 lg:px-8 py-3 hidden md:table-cell">
                        <div class="status{{ $user->id }}">
                            @if ($user->status === null)
                            not defined yet!
                            
                        @endif
                        </div>
                        <div class="relative inline-block align-middle select-none transition duration-200 ease-in">
                            <input data-id="{{$user->id}}" class="toggle-class toggle-checkbox {{-- absolute --}} block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer" name="toggle" type="checkbox"  data-toggle="toggle" data-on="Active" data-off="InActive" {{ $user->status ? 'checked' : '' }}>
                            
                        
                        </div>
                    </td>
                    {{-- role --}}
                    <td class="md:px-2 ml-2 lg:px-8 py-3 hidden md:table-cell">
                        @foreach($user->getRoleNames() as $role)
                            {{ $role }}
                        @endforeach
                    </td>
                    {{-- categories --}}
                    <td class="td{{ $user->id }} md:px-2 lg:px-8 py-3 flex flex-wrap">
                        
                        @foreach($user->categories as $cat) 
                        <div class="card{{ $cat->name }}{{ $user->id }} bg-pink-300 border flex justify-between rounded-md h-auto p-1 m-1">
                            <div class=""> 
                                <div class="capitalize text-gray-900">{{ $cat->name}} </div>   
                            </div>
                            <div class="pl-1 flex items-center content-center">                             
                                <button class="deleteCat" data-user_id="{{ $user->id }}" data-category_name="{{ $cat->name }}" ><i class="text-xs fas fa-times" title="remove"></i></button>   
                            </div>
                        </div>
                        @endforeach
                        {{-- opens a modal window --}}
                        <button class="open" data-id="{{$user->id}}" data-username="{{ $user->username }}" data-categories="{{ $categories }}"><i class="fas fa-plus"></i></button>

                    </td>
                </tr>                    
                @endforeach 
                <!-- MODAL -->
                <div class="modal-overlay" id="overlay" >
                    <div class="modal w-full max-w-md w-full mx-0 bg-white shadow-md rounded-md" id="modal">
                        <div class="flex justify-between items-center bg-pink-300 rounded-t-md px-4">
                            <p class="nameOfUser mt-3 mb-3 text-lg">add category:</p>
                            <button class="close text-lg"><i class="fas fa-times" title="remove"></i></button>
                            
                        </div>
                        <div class="modalContent  p-4">
                        </div>
                    </div>
                </div>
                
                
            </tbody> 
                    
        </table>
        {!! $users->links() !!}

    </div>

<script>
//change status of user with a toggle 
$(function() {
    $('.toggle-class').change(function() {
        
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var user_id = $(this).data('id'); 
         
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('changeUserStatus') }}',
            data: {
                'status': status, 
                'user_id': user_id
            },
            success: function(data){
              console.log(status)
              if(status === 1){
                $(".status"+user_id).html('active');
              }else{
                $(".status"+user_id).html('inactive');
              }
            }
        });
    })
});

//delete selected category from user
$(".deleteCat").click(function()
{    
    var user_id = $(this).data("user_id");
    var category_name = $(this).data("category_name");
    var token = $("meta[name='csrf-token']").attr("content");

    if(!confirm("delete "+category_name)){return false};
    
    $.ajax(
    {
        url: "{{ route('remove-cat') }}",
        type: 'POST',
        data: {
            "user_id": user_id,
            "category_name": category_name,
            "_token": token,
        },
        success:function(response){
            //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
                console.log(response.message);
                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $(".card"+category_name+user_id).remove(); 
                } 
        }   
    });   
});

//function for inline javascript for new dom element /delete selected category from user
function deletecategory(user_id, category_name)
{    
    /* var user_id = $(this).data("user_id");
    var category_name = $(this).data("category_name"); */
    var token = $("meta[name='csrf-token']").attr("content");

    if(!confirm("delete "+category_name)){return false};
    
    $.ajax(
    {
        url: "{{ route('remove-cat') }}",
        type: 'POST',
        data: {
            "user_id": user_id,
            "category_name": category_name,
            "_token": token,
        },
        success:function(response){
            //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
                console.log(response.message);
                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $(".card"+category_name+user_id).remove(); 
                } 
        }   
    });   
};

//Modal window for assigning a category!!
$(".open").on("click", function()
{
    $('.modalContent').empty();

    var user_id = $(this).data('id');
    var categories = $(this).data('categories');
    var username = $(this).data('username');
    var token = $("meta[name='csrf-token']").attr("content");
    
    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");

    //Modal content added with js 
    $(".modalContent").append("<form method='POST' action='#' class='flex-col flex space-y-4'><input type='hidden' name='_token' value='"+token+"'><select class='mx-auto rounded' name='cat' id='cat'></select><button type='submit' class='confirm text-md border border-logogreen text-logogreen hover:border-pink-400 hover:text-pink-400 font-bold mx-1 px-3 py-1 rounded w-32 mx-auto' id='confirmBtn'>Go!</button></form>");

    //close modal when clicking outside the window??

    categories.forEach(category => {
        $('#cat').append("<option value="+category['id']+">"+category['name']+"</option>")
    });
    $('#modalUsername').html(username)
  
    $(document).ready(function () {
        $("form").submit(function (event) {
            var formData = {
                "choosenCat": $("#cat").val(), 
                "user_id": user_id,
                "_token": token
            };

            $.ajax({
                type: "POST",
                url: "{{ route('add-cat') }}",
                data: formData,
                dataType: "json",
                encode: true,
                success:function(response){
                //message via Toaster
                    Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                        },
                        backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                    }).showToast();
                    
                    $('.modalContent').empty();
                    //close modal when assign a category
                    $(".modal-overlay").removeClass("active");
                        
                    if(response.status === 'success')
                    {
                        var choosenCatName = categories[formData['choosenCat']-1]['name'];
                        var choosenCatId = categories[formData['choosenCat']-1]['id'];

                        //DOM manipulation
                        var output = "";

                        output += "<div class='card"+choosenCatName+user_id+" bg-pink-300 border flex justify-between rounded-md h-auto p-1 m-1'><div>";

                        output += "<div class='capitalize text-gray-900'>"+choosenCatName+"</div></div><div class='pl-1 flex items-center content-center'>";

                            
                        output += "<button class='deleteCat' ";
                            
                        var nameoffunction = "deletecategory('"+user_id+"','"+choosenCatName+"')";

                        output += "onclick="+nameoffunction;
                        
                        output += "><i class='text-xs fas fa-times' title='remove'></i></button></div></div>";
                            
                        $(".td"+user_id).prepend(output);

                         
                    }
                }
                
                
            });  

            event.preventDefault();
        });
    });
});


$(".close").on("click", function(){
    $(".modal-overlay").removeClass("active");
    
});
//close modal window on clicking modal-overlay
$(".modal-overlay").on("click", function(e){
    if($(e.target).hasClass("modal-overlay")){
        $(".modal-overlay").removeClass("active");
    }
});

</script>
</x-app-layout>

