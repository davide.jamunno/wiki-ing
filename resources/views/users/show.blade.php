<x-app-layout>
    <x-slot name="header">

        <div class="flex h-9 items-center">
            <div class=" capitalize my-auto text-sm font-medium leading-5 text-gray-900 ">{{ $user->username }}</div>
            
        </div>
        
    </x-slot>
    <div class="">
        
        <div class="max-w-md space-y-5 w-full mx-auto bg-white shadow-md rounded-b-md px-6 py-4 border-t-4 border-pink-200">
            {{-- username --}}
            <div class="flex justify-between items-center">
                <div class="capitalize">
                    <h2 class="font-bold">Username:</h2>
                </div>
                <div class="">
                    {{ $user->username }}
                </div>
            </div>
            {{-- date --}}
            <div class="flex justify-between items-center">
                <div class="capitalize">
                    <h2 class="font-bold">created on:</h2>
                </div>
                <div class="">
                    {{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y')}}
                </div>
            </div> 
            {{-- role --}}
            <div class="flex justify-between items-center">
                <div class="capitalize">
                    <h2 class="font-bold">role:</h2>
                </div>
                <div class="">
                    @foreach ($user->getRoleNames() as $role)
                        <div class="{{ $role }} flex">
                            <p class="mr-8">{{ $role }}</p>
                            <button class="removeRole" data-id="{{ $user->id }}" data-role="{{ $role }}" ><i class="fas fa-minus-circle text-gray-500 hover:text-red-600"></i></button>
                        </div>
                    @endforeach
                </div>
            </div> 
            {{-- status --}}
            <div class="flex justify-between items-center">
                <div class="capitalize">
                    <h2 class="font-bold ">status:</h2>
                </div>
                <div class="flex">
                    <div class="mr-8 status">
                        @if ($user->status === 0)
                                inactive
                                @elseif ($user->status === null)
                                not defined yet!
                                @else
                                active
                                @endif
                    </div>
                    <div class="relative inline-block align-middle select-none transition duration-200 ease-in">
                        <input data-id="{{$user->id}}" class="toggle-class toggle-checkbox {{-- absolute --}} block w-5 h-5 rounded-full bg-white border-2 appearance-none cursor-pointer" name="toggle" type="checkbox"  data-toggle="toggle" data-on="Active" data-off="InActive" {{ $user->status ? 'checked' : '' }}>
                        {{-- <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label> --}}
                    
                    </div>
                </div>
            </div>
            {{-- categories --}}

            {{-- <div class="flex justify-between items-center">
                <div class="capitalize">
                    <h2 class="font-bold">field of work:</h2>
                </div>
                <div class="">
                    @if($categories != null)
                            @foreach($categories as $cat)
                                {{ $cat->name }}  
                            @endforeach  
                    @endif
                </div>
            </div>   --}}

            <div class="flex justify-between items-center">
                <div class="capitalize">
                    <h2 class="font-bold">categories:</h2>
                </div>
                <div class="td{{ $user->id }} md:px-2 lg:px-8 py-3 flex flex-wrap">
                            
                    @foreach($user->categories as $cat) 
                    <div class="card{{ $cat->name }}{{ $user->id }} bg-pink-300 border flex justify-between rounded-md h-auto p-1 m-1">
                        <div class=""> 
                            <div class="capitalize text-gray-900">{{ $cat->name}} </div>   
                        </div>
                        <div class="pl-1 flex items-center content-center">                             
                            <button class="deleteCat" data-user_id="{{ $user->id }}" data-category_name="{{ $cat->name }}" ><i class="text-xs fas fa-times" title="remove"></i></button>   
                        </div>
                    </div>
                    @endforeach
                    {{-- opens a modal window --}}
                    <button class="open" data-id="{{$user->id}}" data-username="{{ $user->username }}" data-categories="{{ $categories }}"><i class="fas fa-plus"></i></button>

                </div>
            </div>
        </div>
    </div>
    <!-- MODAL -->
    <div class="modal-overlay" id="overlay" >
        <div class="modal w-full max-w-md w-full mx-0 bg-white shadow-md rounded-md" id="modal">
            <div class="flex justify-between items-center bg-pink-300 rounded-t-md px-4">
                <h2>add field to: <span class="nameOfUser"></span></h2>
                <button class="close">Close</button>
            </div>
            <div class="modalContent sm:flex sm:justify-between p-4">
            </div>
        </div>
    </div>




<script>
//remove the selected role from user
$(".removeRole").click(function()
{    
    var role = $(this).data("role");
  
    //check if you really want to remove role
    if(!confirm("remove "+role)){return false};
    var user_id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    
                    
    
    $.ajax(
    {
        url: "/remove-role",
        type: 'GET',
        data: {
            "user_id": user_id,
            "_token": token,
            "role" : role
        },
        success:function(response){
        //message via Toaster
            Toastify({
                text: 'role was removed!',
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
                backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
            }).showToast();

            //delete from DOM
            //it doesnt work with "user+"
            $('.'+role).empty(); 
            

        }   
    });
    
}
);

//change status of user with a toggle 
$(function() {
    $('.toggle-class').change(function() {
        
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var user_id = $(this).data('id'); 
         
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('changeUserStatus') }}',
            data: {
                'status': status, 
                'user_id': user_id
            },
            success: function(data){
              console.log(status)
              if(status === 1){
                $(".status").html('active');
              }else{
                $(".status").html('inactive');
              }
            }
        });
    })
});

//delete selected category from user
$(".deleteCat").click(function()
{    
    var user_id = $(this).data("user_id");
    var category_name = $(this).data("category_name");
    var token = $("meta[name='csrf-token']").attr("content");

    if(!confirm("delete "+category_name)){return false};
    
    $.ajax(
    {
        url: "{{ route('remove-cat') }}",
        type: 'POST',
        data: {
            "user_id": user_id,
            "category_name": category_name,
            "_token": token,
        },
        success:function(response){
            //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
                console.log(response.message);
                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $(".card"+category_name+user_id).remove(); 
                } 
        }   
    });   
});

//function for inline javascript for new dom element /delete selected category from user
function deletecategory(user_id, category_name)
{    
    /* var user_id = $(this).data("user_id");
    var category_name = $(this).data("category_name"); */
    var token = $("meta[name='csrf-token']").attr("content");

    if(!confirm("delete "+category_name)){return false};
    
    $.ajax(
    {
        url: "{{ route('remove-cat') }}",
        type: 'POST',
        data: {
            "user_id": user_id,
            "category_name": category_name,
            "_token": token,
        },
        success:function(response){
            //message via Toaster
                Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                    backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                }).showToast();
                console.log(response.message);
                //delete from DOM only if authorised by controller
                if(response.action === 'delete') {
                    //Delete from DOM 
                    $(".card"+category_name+user_id).remove(); 
                } 
        }   
    });   
};

//Modal window for assigning a field!!
$(".open").on("click", function()
{
    $('.modalContent').empty();

    var user_id = $(this).data('id');
    var categories = $(this).data('categories');
    var username = $(this).data('username');
    var token = $("meta[name='csrf-token']").attr("content");
    
    //show modal window (better with .show)??
    $(".modal-overlay").addClass("active");

    //Modal content added with js 
    $(".modalContent").append("<form method='POST' action='#'><input type='hidden' name='_token' value='"+token+"'><select name='cat' id='cat'></select><button type='submit' class='confirm' style='margin-left:0.5em' id='confirmBtn'>Confirm</button></form>");

    //close modal when clicking outside the window??

    categories.forEach(category => {
        $('#cat').append("<option value="+category['id']+">"+category['name']+"</option>")
    });
    $('#modalUsername').html(username)
  
    $(document).ready(function () {
        $("form").submit(function (event) {
            var formData = {
                "choosenCat": $("#cat").val(), 
                "user_id": user_id,
                "_token": token
            };

            $.ajax({
                type: "POST",
                url: "{{ route('add-cat') }}",
                data: formData,
                dataType: "json",
                encode: true,
                success:function(response){
                //message via Toaster
                    Toastify({
                    text: response.message,
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                        },
                        backgroundColor: "linear-gradient(to right, #e074a2, #00a499)"
                    }).showToast();

                    $('.modalContent').empty();
                    //close modal when assign a category
                    $(".modal-overlay").removeClass("active");
                    
                    if(response.status === 'success')
                    {
                        var choosenCatName = categories[formData['choosenCat']-1]['name'];
                        var choosenCatId = categories[formData['choosenCat']-1]['id'];

                        //DOM manipulation
                        var output = "";

                        output += "<div class='card"+choosenCatName+user_id+" bg-pink-300 border flex justify-between rounded-md h-auto p-1 m-1'><div>";

                        output += "<div class='capitalize text-gray-900'>"+choosenCatName+"</div></div><div class='pl-1 flex items-center content-center'>";

                            
                        output += "<button class='deleteCat' ";
                            
                        var nameoffunction = "deletecategory('"+user_id+"','"+choosenCatName+"')";

                        output += "onclick="+nameoffunction;
                        
                        output += "><i class='text-xs fas fa-times' title='remove'></i></button></div></div>";
                            
                        $(".td"+user_id).prepend(output); 
                    }
                }
                
                
            });  

            event.preventDefault();
        });
    });
});


$(".close").on("click", function(){
    $(".modal-overlay").removeClass("active");
    
});
</script>
</x-app-layout>