<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        

        <title>wiki-ing</title>

        <link rel="icon" href="{{ asset('storage'.'/images/WIKIgradient.svg') }}" type="image/x-icon"/>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
        <style>
            .modal-overlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(185, 17, 17, 0.15);
                display: flex;
                justify-content: center;
                align-items: center;
                display: none;
            }
            .modal {
                background-color: white;
                width: 50%;
                min-height: 10rem;
                min-width: 20rem;
                padding: 0 0 2em 0;
            }

            
            .modal-overlay.active{
                display: block;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(185, 17, 17, 0.15);
                display: flex;
                justify-content: center;
                align-items: center;
                z-index: 1;
            }

           
            
        </style>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
        <script src="https://kit.fontawesome.com/fd44956364.js" crossorigin="anonymous"></script>
        
        @stack('head')
        
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100 relative ">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-3 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main class="pb-12">
                {{ $slot }}
            </main>

            <!-- Page Footer -->
            @include('layouts.footer')
        </div>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    </body>

</html>
