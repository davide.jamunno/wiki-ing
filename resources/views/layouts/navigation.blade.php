<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 p-1 sm:p-0 flex items-center">
                    {{-- <a href="{{ route('dashboard') }}"> --}}
                        
                        <img src="{{ asset('storage'.'/images/WIKIgradientTEXT.svg') }}" class="h-14 sm:h-16" />
                
                        
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-2  md:space-x-4 lg:space-x-8 sm:-my-px  md:ml-5 md:flex">

                    <x-nav-link :href="route('favourites')" :active="request()->routeIs('favourites')">
                        {{ __('Favourites') }}
                    </x-nav-link>


                    <x-nav-link :href="route('search')" :active="request()->routeIs('search', 'searchCategory')">
                        {{ __('Search') }}
                    </x-nav-link>

                    <x-nav-link :href="route('readinglists')" :active="request()->routeIs('readinglists', 'readinglists.shared')">
                        {{ __('Readinglists') }}
                    </x-nav-link>

                    @can('write articles')
                    <x-nav-link :href="route('article.create')" :active="request()->routeIs('article.create')">
                        {{ __('Write') }}
                    </x-nav-link>
                    @endcan
                    
                    @can('edit articles')
                    <x-nav-link :href="route('myarticles')" :active="request()->routeIs('myarticles')">
                        {{ __('My articles') }}
                    </x-nav-link>
                    @endcan

                    {{-- @can('delete articles')
                    <x-nav-link :href="route('statusZero')" :active="request()->routeIs('statusZero')">
                        {{ __('Inactive articles') }}
                    </x-nav-link>
                    @endcan --}}
                    
                    
                    @can('manage users')
                    <x-nav-link :href="route('users')" :active="request()->routeIs('users')">
                        {{ __('Admin') }}
                    </x-nav-link>
                    @endcan
                    
                    {{-- @can('create categories')
                    <x-nav-link :href="route('categories')" :active="request()->routeIs('categories')">
                        {{ __('Categories') }}
                    </x-nav-link>
                    @endcan --}}

        
                    
                </div>
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden md:flex md:items-center md:ml-6">
                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button class="flex items-center text-md font-bold text-logogreen hover:text-pink-400 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div> {{ Auth::user()->username }}</div>

                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Log out') }}
                            </x-dropdown-link>
                        </form>
                    </x-slot>
                </x-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="p-1 sm:p-0 -mr-2 flex items-center md:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center rounded-md text-logogreen hover:text-pink-400 hover:bg-pink-200 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-16 w-16" stroke="currentcolor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden md:hidden">
           <!-- Responsive Settings Options -->
           <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center content-center px-4">
               
                
                    <div class="font-medium text-base text-logogreen">hi, {{ Auth::user()->username }} </div>
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
            
                        <x-nav-link :href="route('logout')"
                                onclick="event.preventDefault();
                                            this.closest('form').submit();">
                            {{ __(' -> Log out') }}
                        </x-nav-link>
                    </form>
                    
                
            </div>
        </div>
        <div class="flex flex-col pt-2 pb-3 space-y-1">
            
            <x-nav-link :href="route('favourites')" :active="request()->routeIs('favourites')">
                {{ __('Favourites') }}
            </x-nav-link>

            <x-nav-link :href="route('search')" :active="request()->routeIs('search')">
                {{ __('Search') }}
            </x-nav-link>

            <x-nav-link :href="route('readinglists')" :active="request()->routeIs('readinglists')">
                {{ __('Readinglists') }}
            </x-nav-link>

            @can('write articles')
            <x-nav-link :href="route('article.create')" :active="request()->routeIs('article.create')">
                {{ __('Write') }}
            </x-nav-link>
            @endcan
            
            @can('edit articles')
            <x-nav-link :href="route('myarticles')" :active="request()->routeIs('myarticles')">
                {{ __('My articles') }}
            </x-nav-link>
            @endcan

            <br>

            @can('manage users')
            <x-nav-link :href="route('users')" :active="request()->routeIs('users')">
                {{ __('Manage Users') }}
            </x-nav-link>
            @endcan

            @can('delete articles')
            <x-nav-link :href="route('statusZero')" :active="request()->routeIs('statusZero')">
                {{ __('Inactive articles') }}
            </x-nav-link>
            @endcan
            
            @can('create categories')
            <x-nav-link :href="route('categories')" :active="request()->routeIs('categories')">
                {{ __('Categories') }}
            </x-nav-link>
            @endcan

            
            
        </div>

     
    </div>
</nav>
