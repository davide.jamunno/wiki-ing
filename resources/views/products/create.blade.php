<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products/create') }}
        </h2>
    </x-slot>


<p>hallo / create</p>

<form action="{{ route('products.store') }}" method="post">

    {{-- verify that the token in the request input matches the token stored in the session. When these two tokens match, we know that the authenticated user is the one initiating the request. --}}
    @csrf

    
    <div class="form-group">
        <label for="name">name</label>
        <input type="text" class="form-control" name="name" placeholder="write the product name here">
    </div>


    <div class="form-group">
        <label for="detail">detail</label>
        <textarea type="text" class="form-control" rows="8" name="detail" placeholder="write your detail here" ></textarea>
    </div>
    
    
    <div class="form-group">
        <label for="category">category</label>
        <textarea type="text" class="form-control" rows="8" name="category" placeholder="write your category here" ></textarea>
    </div>

    

    <button type="submit" class="btn btn-primary">Save</button>

    
</form>
</x-app-layout>