<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    All Products!!
                </div>
                
                <table>
                    <tr>
                        <th>product</th>
                        <th>created by</th>
                        <th>action</th>
                        <th>action</th>
                    </tr>
{{-- remove super admin role from logged user--}}
{{--     @php
    $user = auth()->user();
    $user->removeRole('super-admin');
    @endphp --}}
                    
                    @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ $users[$product->user_id -2]->username }}</td>
                        @can('delete articles')
                        <td>delete</td>
                        
                        @endcan

                        @if(auth()->user()->can('edit articles') && auth()->user()->id == $product->user_id)
                        <td>edit</td>

                        @else
                        <td>--</td>
                        @endif
                        
                    </tr>
                    
                    @endforeach
                    
                    
                </table>
            </div>
        </div>
    </div>
</x-app-layout>