<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        
        //user+
        Permission::create(['name' => 'write articles']);
        Permission::create(['name' => 'edit articles']);
        Permission::create(['name' => 'share articles']);
        //admin only
        Permission::create(['name' => 'delete articles']);
        Permission::create(['name' => 'manage users']);
        Permission::create(['name' => 'create category']);
        


        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'user']);
        //no permissions for user, just read if logged in!

        $role2 = Role::create(['name' => 'user+']);
        $role2->givePermissionTo('write articles');
        $role2->givePermissionTo('edit articles');
        $role2->givePermissionTo('share articles');
        

        $role3 = Role::create(['name' => 'super-admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

         
    }
}
