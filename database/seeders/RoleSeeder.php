<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Role::create([
            'name' => 'Super Admin',
            'guard_name' => 'web',
          ]);
           */
        $user = \App\Models\User::factory()->create([
            'username' => 'non admin',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole(Role::findByName('user+'));
    }
}
