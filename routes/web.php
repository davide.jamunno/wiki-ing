<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\FavouriteController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReadinglistController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('login');
})->middleware('guest'); */




//middleware
//al routes that need to be protected from non logged in users 
Route::middleware(['auth'])->group(function () {
    /* Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard'); */

    //articles
    /* Route::get('/articles', [ArticleController::class, 'index'])->name('articles'); */   
    Route::get('/articles/{id}', [ArticleController::class, 'show'])->whereNumber('id')->name('article.show'); 
    Route::get('/search', [ArticleController::class, 'search'])->name('search'); 
    Route::get('/search/category', [ArticleController::class, 'searchByCategory'])->name('searchCategory');

    //favourites
    Route::get('/favourites', [FavouriteController::class, 'index'])->name('favourites');   
    Route::post('/favourites/add', [FavouriteController::class, 'addToFavourites'])->whereNumber('id')->name('addToFavourites');
    Route::post('/favourites/remove', [FavouriteController::class, 'removeFromFavourites'])->whereNumber('id')->name('removeFromFavourites');

    //reading List
    Route::get('/readinglists', [ReadinglistController::class, 'index'])->name('readinglists');
    Route::get('/readinglists/shared', [ReadinglistController::class, 'indexSharedLists'])->name('readinglists.shared');
    Route::get('/readinglists/read', [ReadinglistController::class, 'readIt'])->name('readinglists.readIt');
    

});

//spatie
Route::group(['middleware' => ['permission:write articles']], function () {
    //articles
    Route::get('/myarticles', [ArticleController::class, 'myArticles'])->name('myarticles');   
    Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
    Route::post('/article/create', [ArticleController::class, 'store'])->name('article.store');
    Route::POST('/article/upload_image', [ArticleController::class, 'upload_image'])->name('article.upload_image');
    
    //reading list
    Route::get('/readinglist/create', [ReadinglistController::class, 'create'])->name('readinglist.create');
    Route::post('/readinglist/create', [ReadinglistController::class, 'store'])->name('readinglist.store');
    Route::post('/readinglist/createwitharticle', [ReadinglistController::class, 'createwitharticle'])->name('readinglist.createwitharticle');
    Route::post('/readinglist/save-article', [ReadinglistController::class, 'saveArticle'])->name('readinglist.save-article');
    Route::post('/readinglist/remove', [ReadinglistController::class, 'removeFromList'])->name('removeFromList');
    Route::post('/readinglist/share', [ReadinglistController::class, 'shareList'])->name('shareList');
    //delete reading list
    Route::delete('/readinglist/{id}', [ReadinglistController::class, 'deleteList']);
    //remove reading list sharing
    Route::post('/readinglist/deleteListFromUser', [ReadinglistController::class, 'deleteListFromUser'])
               ->middleware('auth')
               ->name('remove-list');
    
});

Route::group(['middleware' => ['permission:edit articles']], function () {  
    Route::get('/articles/{id}/edit', [ArticleController::class, 'edit'])->name('article.edit'); 
    Route::patch('/articles/{id}', [ArticleController::class, 'update'])->name('article.patch'); 
    Route::post('/articles/deletePicture', [ArticleController::class, 'delete_picture'])->name('deletePicture');
});

Route::group(['middleware' => ['permission:delete articles']], function () {
    
    /* Route::delete('/articles/{id}', [ArticleController::class, 'destroy'])->whereNumber('id'); */
    Route::post('/articles/delete', [ArticleController::class, 'destroy'])->name('article.delete');
    Route::get('/articles/status-zero', [ArticleController::class, 'statusZeroList'])->name('statusZero');
    Route::get('/articles/change-status', [ArticleController::class, 'changeArticleStatus'])->name('changeArticleStatus'); 
    Route::POST('/articles/change-status-active', [ArticleController::class, 'setArticleStatusToOne'])->name('setArticleStatusToOne'); 
    Route::POST('/articles/change-comment', [ArticleController::class, 'changeArticleComment'])->name('changeArticleComment'); 

    
    
    
});


Route::group(['middleware' => ['permission:create category']], function () {
    Route::get('/categories', [CategoryController::class, 'index'])->name('categories');
    Route::get('/category/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/category/create', [CategoryController::class, 'store'])->name('category.store');
});



require __DIR__.'/auth.php';
