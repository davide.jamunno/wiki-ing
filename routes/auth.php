<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\AssignRoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


 Route::group(['middleware' => ['permission:manage users']], function () { 



    Route::get('/users', [UserController::class, 'index'])
                ->middleware('auth')
                ->name('users');
    Route::get('/users/{id}', [UserController::class, 'show'])
                ->middleware('auth');

    Route::get('/change-status', [UserController::class, 'changeUserStatus'])->name('changeUserStatus'); 
                
    Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware('auth')
                ->name('register');

    Route::post('/register', [RegisteredUserController::class, 'store'])
                ->middleware('auth');

    Route::get('/assign-role', [AssignRoleController::class, 'assignRoleCreate'])
                ->middleware('auth')
                ->name('assign-role');

    Route::post('/assign-role', [AssignRoleController::class, 'assignRoleStore'])
                ->middleware('auth');

    Route::get('/remove-role', [AssignRoleController::class, 'removeRole'])
                ->middleware('auth')
                ->name('remove-role');
   

   Route::post('/category_user/add', [CategoryController::class, 'addCategoryToUser'])
               ->middleware('auth')
               ->name('add-cat');

   Route::post('/category_user/remove', [CategoryController::class, 'deleteCategoryFromUser'])
               ->middleware('auth')
               ->name('remove-cat');
                
 });

 Route::get('/', [AuthenticatedSessionController::class, 'create'])
 ->middleware('guest')
 ->name('login');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');

